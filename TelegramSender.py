import telepot
import json,requests,time
from dbmanager import dbmanage
from stringutil import sUtil
from bs4 import BeautifulSoup as soup
from threading import Thread
from queue import Queue

class tel_bot(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.Endflag = False
		self.m_dbm = dbmanage()
		self.m_queue = Queue()

		with open('load_id.json') as data_file:    
			self.users_data = json.load(data_file)
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			self.users_data[id_str]['inst'] = telepot.Bot(self.users_data[id_str]['token'])
			self.users_data[id_str]['inst'].message_loop(self.msg_handle)
		#
		self.start()

	def run(self):
		while self.Endflag is not True:
			if self.m_queue.qsize() is not 0:
				# [bot,'photo',chat_id,url,message]
				# [bot,'text',chat_id,message]
				idmessage = self.m_queue.get()
				if len(idmessage) < 4 : continue
				try:
					if idmessage[1] == 'photo':
						idmessage[0].sendPhoto(idmessage[2],idmessage[3],caption=idmessage[4])
					elif idmessage[1] == 'text':
						idmessage[0].sendMessage(idmessage[2],idmessage[3])
				except:
					print('텔레그램 전송오류 재전송합니다.')
					try:
						if idmessage[1] == 'photo':
							idmessage[0].sendPhoto(idmessage[2],idmessage[3],caption=idmessage[4])
						elif idmessage[1] == 'text':
							idmessage[0].sendMessage(idmessage[2],idmessage[3])
					except:
						print('텔레그램 재전송에 실패하였습니다.')
			time.sleep(0.25)

	#큐삽입
	def insert_queue(self,aitem):
		self.m_queue.put(aitem)

######
	def sendMessage_all(self,astr):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			target_bot = self.users_data[id_str]['inst']
			target_id = self.users_data[id_str]['id']
			self.insert_queue([target_bot,'text',target_id,astr])

	def sendPhoto_all(self,aurl,amessage):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			target_bot = self.users_data[id_str]['inst']
			target_id = self.users_data[id_str]['id']
			self.insert_queue([target_bot,'photo',target_id,aurl,amessage])			

	def msg_handle(self,msg):
		content_type, chat_type, chat_id = telepot.glance(msg)	#메시지에서 추출하는부분
		#print(content_type, chat_type, chat_id)
		if content_type == 'text':
			#누가보낸건지 찾아냄
			target_bot = None
			for idx,md in enumerate(range(len(self.users_data))):
				id_str = 'user'+ str(idx+1)
				if str(self.users_data[id_str]['id']) == str(chat_id):
					target_bot = self.users_data[id_str]['inst']

			#회사코드로 검색을 요청한 경우
			if sUtil.is_only_digit(str(msg['text'])):
				num_list = self.m_dbm.get_corp_codes(str(msg['text']))
				mk_words = ''

				if len(num_list) == 0:
					self.insert_queue([target_bot,'text',target_id,'해당 종목을 찾지 못하였습니다.'])
				else:
					for nrow in num_list: #한개만 왔을꺼임					
						mk_words += self.get_corp_info(str(nrow[0]),str(nrow[1]))
					photo_url = 'http://cichart.moneta.kr/pax/chart/candleChart/V201200/paxCandleChartV201200Daily.jsp?abbrSymbol=' + str(msg['text'])
					self.insert_queue([target_bot,'photo',chat_id,photo_url,''])
					self.insert_queue([target_bot,'text',chat_id,mk_words])

			else: #회사명으로 검색을 요청한 경우
				ret_list = self.m_dbm.get_corp_names(str(msg['text']))
				if len(ret_list) == 0:
					self.insert_queue([target_bot,'text',chat_id,"'"+ msg['text'] +"' 의 검색결과 해당 회사명이 존재하지 않습니다."])
				else:
					ret_word = ""				
					for row in ret_list:
						tw = str(row[0])+ '('+ str(row[1]) +')'+ ' ' + str(row[2]) + '\r\n\r\n'
						ret_word += tw
					self.insert_queue([target_bot,'text',chat_id,ret_word])					
	
	#회사정보 추출
	def get_corp_info(self,aname,acode):
		request_word = 'http://finance.naver.com/item/coinfo.nhn?code='
		request_word += acode
		ret_list = []
		ret_list.append(acode)
		req = requests.get(request_word)
		html = req.text
		asoup = soup(html, 'html.parser')

		#[현재가,거래량,시가총액,총주식수,52주최고,52주최저,PER,EPS]
		#현재가,거래량
		pdiv = asoup.find_all("div",{"class","new_totalinfo"})
		for ppdiv in pdiv:
			 for adl in ppdiv.find_all("dl"):
			 	for add in adl.find_all("dd"):
			 		try:
			 			if "현재가" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 			if "거래량" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 		except ValueError: pass

		#시가총액,총주식수,52주최고,52주최저,PER,EPS
		ncounter = 0
		pdiv = asoup.find_all("div",{"id","aside_invest_info"})
		for ppdiv in pdiv:
			for ptable in ppdiv.find_all("table"):
				for ptd in ptable.find_all("td"):
					for pem in ptd.find_all("em"):
						try:
							if [0,2,12,13,14,15].index(ncounter) >= 0 :
								ret_list.append(sUtil.make_digit(pem.text))
						except ValueError: pass
						ncounter+=1

		add_coinfo = ''
		pdiv = asoup.find_all("div",{"id","summary_info"})
		for ppdiv in pdiv:
			for ptag in ppdiv.find_all("p"):
				add_coinfo += ptag.text
				
		#사번 현재가 거래량 시가총액(억) 상장주식수 52주최고 52주최저 PER EPS
		ret_info_words = ''
		ret_info_words += aname + '('+ acode + ')' + '\r\n'
		ret_info_words += '현재가 = ' + ret_list[1] + '\r\n'
		ret_info_words += '현재거래량 = ' + ret_list[2] + '\r\n'
		ret_info_words += '시가총액(억원) = '+ ret_list[3] + '\r\n'
		ret_info_words += '상장주식수 = ' + ret_list[4] + '\r\n'
		ret_info_words += '52주최고가 = ' + ret_list[5] + '\r\n'
		ret_info_words += '52주최저가 = ' + ret_list[6] + '\r\n'
		ret_info_words += 'PER = ' + ret_list[7] + '\r\n'
		ret_info_words += 'EPS = ' + ret_list[8] + '\r\n'
		ret_info_words += '회사개요 = '+'\r\n'+add_coinfo + '\r\n'
		
		return ret_info_words


if __name__ == '__main__':
	aurl = 'https://i.ytimg.com/vi/6Y7MiujpVdY/maxresdefault.jpg'
	simbol = 'http://cichart.moneta.kr/pax/chart/candleChart/V201200/paxCandleChartV201200Daily.jsp?abbrSymbol=' + '053290'
	#tel_bot().sendMessage_alone('안녕하시오 테스트이오')
	tel_bot()
	#a = tel_bot()
	#while True:	pass