#1,2차 필터링을 진행
from bs4 import BeautifulSoup as soup
import sqlite3,requests
import time,datetime
from stringutil import sUtil
import dbmanager as dbm

class CodeFilter:
	def __init__(self):
		self.m_dbm = dbm.dbmanage()

	#네이버에서 긁어 온다..  8가지
	def getCoinfo(self,acode):
		request_word = 'http://finance.naver.com/item/coinfo.nhn?code='
		request_word += acode
		ret_list = []
		ret_list.append(acode)
		req = requests.get(request_word)
		html = req.text
		asoup = soup(html, 'html.parser')

		#[현재가,거래량,시가총액,총주식수,52주최고,52주최저,PER,EPS]
		#현재가,거래량
		pdiv = asoup.find_all("div",{"class","new_totalinfo"})
		for ppdiv in pdiv:
			 for adl in ppdiv.find_all("dl"):
			 	for add in adl.find_all("dd"):
			 		try:
			 			if "현재가" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 			if "거래량" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 		except ValueError: pass

		#시가총액,총주식수,52주최고,52주최저,PER,EPS
		ncounter = 0
		pdiv = asoup.find_all("div",{"id","aside_invest_info"})
		for ppdiv in pdiv:
			for ptable in ppdiv.find_all("table"):
				for ptd in ptable.find_all("td"):
					for pem in ptd.find_all("em"):
						try:
							if [0,2,12,13,14,15].index(ncounter) >= 0 :
								ret_list.append(sUtil.make_digit(pem.text))
						except ValueError: pass
						ncounter+=1


		#해당종목이 관리종목인지 여부
		warn_word = ['관리종목','거래정지','투자위험']
		desc = asoup.find_all("div",{"class","description"})
		for pdesc in desc:
			for aspan in pdesc.find_all("span"):
				if len(aspan.text.strip()) > 0 and aspan.text.strip() in warn_word:
					ret_list.append(aspan.text.strip())

		return ret_list

	def update_2ndFilter(self,codelist):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		#[종목코드,전일가(bday_price),거래량(bday_volume),시가총액(totvalue),총주식수(stock_count)
		#,52주최고(max52),52주최저(min52),PER(per),EPS(eps)]
		seq = 0
		for row in codelist:
			print('%d번째종목(%s) 삽입중..'%(seq,row[0]))
			seq+=1
			info = self.getCoinfo(row[1])
			if len(info) < 9:   #특별한 아디들은 처리하지 않는다..
				continue

			#투자경고 나 거래정지 종목은 삽입하지 않는다.
			if len(info) > 9:
				if '거래정지' in info[9] or '투자위험' in info[9]:
					continue

			#per를 기준으로 나오지 않거나(1년도안된 신규회사).. 음수(적자기업)면 투자점수는없고 나머지는10점을 획득
			#per를 기준으로 전자나온 기업을 발라내는것을 일단 취소한다 재무분석에서 다시 제대로 한다.
			try:
				if float(info[7]) < 0:	#per가 마이너스인 기업		
					curr.execute("UPDATE tb_codes SET bday_price=(?),bday_volume=(?),totvalue=(?),stock_count=(?),max52=(?),min52=(?),per=(?),eps=(?) WHERE codenumber=(?)"
					, (int(info[1]),int(info[2]),int(info[3]),int(info[4]),int(info[5]),int(info[6]),info[7],info[8],info[0]))	
					continue
					
			except ValueError:
				continue

			#1점으로 다시 세팅하고 다시 제대로 한다..
			curr.execute("UPDATE tb_codes SET filter_step=(?),invest_score=(?),bday_price=(?),bday_volume=(?),totvalue=(?),stock_count=(?),max52=(?),min52=(?),per=(?),eps=(?) WHERE codenumber=(?)"
			, (2,1,int(info[1]),int(info[2]),int(info[3]),int(info[4]),int(info[5]),int(info[6]),info[7],info[8],info[0]))	

		conn.commit()
		curr.close()
		conn.close()

	#매출증가 점수 계산 매출감소가 있었으면 5점감소 매출이 마이너스면 20점감소
	def get_score_sale(self,alist):
		manage_list = []
		before = 0.0
		idx,minus_flag = 0,0
		ret_score = 100
		for aw in alist:
			idx += 1
			now = sUtil.make_number(aw)
			if now < 0 : minus_flag += 1
			if idx == 1:
				before = now
				continue
			gap = now - before
			if(gap < 0) : ret_score -= 10
			before = now
			manage_list.append(gap)

		ret_score += (minus_flag * -20)		
		return (manage_list,ret_score)

	#순이익 점수 계산
	#적자가 있으면 0점 마이너스 성장이면 낮은점수 마이너스 성장 변폭이크면 중간점수
	#무난하게 성장했으면 100점
	def get_score_profit(self,alist):
		manage_list = []
		before = 0.0
		idx,minus_flag = 0,0
		ret_score = 100
		for aw in alist:
			idx += 1
			diff = []
			now = sUtil.make_number(aw)
			if now < 0 : minus_flag += 1
			if idx == 1:
				before = now
				continue
			gap = now - before
			diff.append(gap)

			if before != 0: #0으로 나누기 회피
				if before < now:
					diff.append(abs(round((gap / before),2)))
				elif before > now:
					diff.append(abs(round((gap / before),2))*-1)
				elif before == now:
					diff.append(0.0)
			else:
				diff.append(0.0)
			
			before = now
			#전분(연)기 대비 수익이 감소했다면 차감
			if diff[0] < 0 : ret_score -= 5
			#전분(연)기 대비 손실폭이 크면 차감 (영업이익은 이럴수 있다. 다른데다가 돈썻으면)
			if diff[1] < -1 : ret_score -= 5
			manage_list.append(diff)

		#적자가 났으면 차감
		ret_score += (minus_flag * -20)
		if ret_score < 0 : ret_score = 0		
		return (manage_list,ret_score)

	#재무재표 순서상으로 순차적으로 증가하면 높은점수 감소하면 낮은점수 비슷비슷 하면 살짝 낮은점수
	#기존 적자기업이면 20점 획득함 매출증가라면 20점추가 순이익증가라면 30점추가
	def update_3rdFilter(self,benefit_codelist):
		request_word = 'http://finance.naver.com/item/main.nhn?code='
		ret_list = []
		year_sale, year_earn, quter_sale, quter_earn = [],[],[],[]

		#리스트 순회
		for row in benefit_codelist:
			corp_code = str(row[1])
			for_ret = []
			for_ret.append(corp_code)

			try:
				req = requests.get(request_word + corp_code,timeout=10)
				html = req.content
				asoup = soup(html,'html.parser')
			except:
				print('Request 익셉션발생 naver')
				continue

			#리스트 순회해야함
			#insert_flag 0이면 연간매출 1이면 분기매출 2면 연간순이익 3이면 분기순이익
			insert_flag = 0
			print('회사명(%s) '%(row[0],),end='')
			pdiv = asoup.find("th",{"class","th_cop_anal8"})
			for atd in pdiv.parent.find_all('td'):
				if insert_flag == 0:
					if len(atd.text.strip()) > 0 : year_sale.append(atd.text.strip())
				elif insert_flag == 1:
					if len(atd.text.strip()) > 0 : quter_sale.append(atd.text.strip())

				if 't_line' in atd.attrs['class']: 
					insert_flag = 1
				elif 'last' in atd.attrs['class']:
					insert_flag = 2
					break

			#여기서 완전실수했다.. 당기순이익이 아니라 영업이익이였다!!
			qdiv = asoup.find("th",{"class","th_cop_anal9"})
			for atd in qdiv.parent.find_all('td'):
				if insert_flag == 2:
					if len(atd.text.strip()) > 0 : year_earn.append(atd.text.strip())
				elif insert_flag == 3:
					if len(atd.text.strip()) > 0 : quter_earn.append(atd.text.strip())

				if 't_line' in atd.attrs['class']: 
					insert_flag = 3
				elif 'last' in atd.attrs['class']: 
					break

			#매출증가라면 25점추가 순이익증가라면 25점추가
			#다만 최근분기에서 매출이 적자이거나 순이익이 적자라면 점수를 획득하지 않음
			full_score_sale, full_score_earn = 25,25
			if len(year_sale) < 1: sale_score = 0
			else:
				set_sale = self.get_score_sale(year_sale)
				sale_score = full_score_sale * (int(set_sale[1])/100)
				if len(quter_sale) > 0 : 
					if sUtil.make_number(quter_sale[len(quter_sale)-1]) < 0: sale_score = 0

			if len(year_earn) < 1 : earn_score = 0
			else:
				set_earn = self.get_score_profit(year_earn)
				earn_score = full_score_earn * (int(set_earn[1])/100)
				if len(quter_earn) > 0 :
					if sUtil.make_number(quter_earn[len(quter_earn)-1]) < 0: earn_score = 0
			


			#최근분기 PBR 1.75배 이하, 최근년도 부채비율 100%이하, 최근분기 영업활동 현금흐름 1억이상 
			#재무점수
			finance_score = int(sale_score) + int(earn_score)	

			#최근분기 pbr 1.75이하			
			cth = asoup.find("table",{"class","per_table"}).find_all("td")
			sc_pbr = "None"
			for innc in cth:	#td안쪽
				emm = innc.findNext()	#첫번째 태그에 있다.			
				eatt = emm.attrs
				try:
					if "_pbr" == eatt['id']:
						sc_pbr = emm.text
				except KeyError: pass
				if sc_pbr is not "None": break

			
			#pbr 받아오지 못하는 경우
			if sc_pbr is not "None":
				if float(sUtil.make_number(sc_pbr)) >= 1.75: finance_score = 0


			#BPS = (최근분기 자본총계  / 총주식수)	
			#PBR = (현재가  / BPS)

			
			#최근년도 부채비율 100%이하
			bth = asoup.find("th",{"class","th_cop_anal14"}).parent.findNext('td')
			brate = 0.0
			for btd in bth.parent.find_all('td'):
				if len(btd.text.strip()) != 0 and btd.text.strip() != '-':
					brate = float(sUtil.make_digit(btd.text.strip()))
				if 'cell_strong' in btd.attrs['class']:	break
			if brate > 100.0 :	finance_score = 0

			#NICE재무정보로 넘어가기 전에 연결인지 별도인지 판단한다 연결이 많으면 1로 개별이 많으면 0			
			lnk_ch = 1
			lkth0 = asoup.find_all('th',{'class','link_ifrs'})
			lkth1 = asoup.find_all('th',{'class','link_ifrs2'})			
			if len(lkth0) >= len(lkth1): lnk_ch = 1
			else : lnk_ch = 0
			
			#NICE재무정보로 넘어가줘야한다!!!!!!  아래의 조건을 추가로 검색하기위해서
			try:
				nreq = requests.get('http://media.kisline.com/fininfo/mainFininfo.nice?paper_stock=' + corp_code + '&nav=4',timeout=10)
				nhtml = nreq.content
				nasoup = soup(nhtml,'html.parser')
			except:
				print('request 익셉션 발생 NICE')
				continue
			
			####최근분기 유동비율 150% 이상  
			urate = []
			ud_word  = 'Fin' + str(lnk_ch)
			npdiv = nasoup.findAll('div', attrs={'id':ud_word})
			for arow in npdiv:				
				for path in arow.find_all('th'):
					if path.text.strip() == '유동비율':
						for tdpart in path.parent:
							if tdpart.name == 'td':
								urate.append(tdpart.text.strip())		

			#일부회사는 유동비율이 없는것 같다. 금융회사 같은곳 ifrs연결을 지원하지 않는회사도 있다.			
			if len(urate) > 0:
				#분기보고서를 재출하지 않은경우 값은 '-' 이다.
				if urate[-1] == '-':
					if urate[-2] == '-': finance_score = 0
					elif float(urate[-2]) < 150: finance_score = 0						
				else:
					if float(urate[-1]) < 150 :	finance_score = 0	



			#최근분기 이잉익여금이  / 최근분기 자본총계   0.3 이상



			####최근분기 영업활동 현금흐름 1억이상
			flv = []
			fd_word = 'Hin' + str(lnk_ch)
			fpdiv = nasoup.findAll('div', attrs={'id':fd_word})
			for frow in fpdiv:
				for fath in frow.find_all('th'):
					if fath.text.strip() == '영업활동으로인한현금흐름':
						for ftdpart in fath.parent:
							if ftdpart.name == 'td':
								flv.append(ftdpart.text.strip())

			if len(flv) > 0:	#최근분기가 없으면 최근년도이다.
				if flv[-1] == '-': 
					#2번째것도 없으면 0점이다.
					if flv[-2] == '-': finance_score = 0
					elif float(sUtil.make_number(flv[-2])) < 100: finance_score = 0
				else:	#영업활동현금흐름 1억이상이고 단위가 백만원이니까 100 이상이 되어야한다.
					if float(sUtil.make_number(flv[-1])) < 100:
						finance_score = 0
			

			#포괄속익 3년연속 흑자  최근4분기의 합은 1이상 최근2분기 연속적자는 안됨			
			galist = []
			gd_word = 'Pin' + str(lnk_ch)
			gadiv = nasoup.findAll('div', attrs={'id':gd_word})
			for garow in gadiv:
				for gath in garow.find_all('th'):
					if gath.text.strip() == '포괄손익':
						for gatdpart in gath.parent:
							if gatdpart.name == 'td':
								galist.append(gatdpart.text.strip())
			
			#연간으로만 재출하는 회사도 있다.. 그래서 5개인데 이건그냥 패스
			if len(galist) == 10:				
				if galist.count('-') == 2 and galist[4] == galist[9]:
					#최근분기만 제출하지 않은경우 하나씩 땡겨서처리
					num_galist = []	#스트링유틸이 '-'는 0으로 처리
					for nga in galist: num_galist.append(sUtil.make_number(nga))
					ga3y = (num_galist[1]>0) and (num_galist[2]>0) and (num_galist[3]>0)
					q4sum = ((num_galist[5]+num_galist[6]+num_galist[7]+num_galist[8]) > 0)
					lt2g = num_galist[7] > 0 and num_galist[8] > 0
					if not (ga3y and q4sum and lt2g):
						finance_score = 0
				elif galist.count('-') > 2: #전체를 완전히 받을수 없는경우
					finance_score = 0
				elif '-' not in galist:			
					num_galist = []
					for nga in galist: num_galist.append(sUtil.make_number(nga))
					ga3y = (num_galist[2]>0) and (num_galist[3]>0) and (num_galist[4]>0)
					q4sum = ((num_galist[6]+num_galist[7]+num_galist[8]+num_galist[9]) > 0)
					lt2g = num_galist[8] > 0 and num_galist[9] > 0
					if not (ga3y and q4sum and lt2g):
						finance_score = 0

			########################
			for_ret.append(finance_score)
			print('회사코드(%s)에 점수(%s)획득'%(for_ret[0],for_ret[1]))
			year_sale, year_earn, quter_sale, quter_earn = [],[],[],[]
			ret_list.append(for_ret)

		#가져온 점수들을 업데이트한다.
		self.m_dbm.add_corp_score(ret_list)


if __name__ == '__main__':
	tmp = CodeFilter()
	#print(tmp.getCoinfo('08537M'))
	#tmp.update_2rdFilter([['SBI','']])
	tmp.update_3rdFilter([['유유제약2우B', '000227', 'KOSPI', 20170627, 20, 7500, 90901, 40100, 4990, 3512, 46822295, '26.04', '288'],])
