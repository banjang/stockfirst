from bs4 import BeautifulSoup as soup
import sqlite3,requests
import time,datetime
from Schedule import timeUtil
import FinanceFilter,ChartFilter,VerifyFilter
import dbmanager as dbm
import stringutil as su

class CodeGetter:
	def __init__(self):
		self.filterword = ['하나머스트','키움스팩','케이티비스팩','케이비제'
		,'유안타제','엔에이치스팩','미래에셋제','골든브릿지제','신한BNPP','신한 '
		,'삼성 ','미래에셋 ','TRUE ','TIGER ','QV ','KOSEF','KODEX'
		,'KINDEX','KBSTAR ','KB ','ARIRANG','케이비제','케이비드림','케이티비'
		,'하이골드','하나티마크','이지스코어부동산','신한BNPP','신한 '
		,'미래에셋 ','TRUE ','TREX ','IBKS','SMART ','KTOP ','한화에이스'
		,'하이제','하이에이아이','스팩','맵스','멀티에셋','마이티','ETN','KTOP'
		,'KB서울햇빛발전소특별자산','PAM부동산','동북아','동아쏘시오홀딩스','동양 '
		,'두산건설 ','두산중공업 ','마이다스 ','메리츠베트남주식혼합','바다로'
		,'부산주공 ','아비스타 ','엔케이 ','파워 ']

		#웹필터
		self.m_dbm = dbm.dbmanage().crate_all_table() #최초에 한번 create 체크
		self.m_webFileter = FinanceFilter.CodeFilter()
		self.m_chartFilter = ChartFilter.chartgrapper()
		self.m_verifyFilter = VerifyFilter.verifyfilt()

	#오늘날자로 종목들이 들어가있는지 여부
	def check_insert_code(self):
		now = datetime.datetime.now()
		gaz = now.hour * 100 + now.minute
		if not (gaz >= 900 and gaz <= 1920): return False
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		#테이블에서 날짜를 글어와서 비교후 리턴
		curr.execute("SELECT timestp FROM tb_codes limit 1")
		row = curr.fetchall()
		db_date = str(row[0]).replace('(','').replace(',','').replace(')','')
		curr.close()
		conn.close()
		if db_date == timeUtil.getDateStamp(): return "inserted"
		else: return "not_inserted"

	#긁어올때 filter_word라면 필터링
	def check_filter_word(self,aword):
		for aw in self.filterword:
			try:
				if aword.index(aw) >= 0 : return True
			except ValueError:
				continue
		return False

	#종목의 현재가를 가져와서 리턴한다.
	def get_now_price(self,acode):
		req_word = 'http://finance.naver.com/item/coinfo.nhn?code=' + str(acode)		
		req = requests.get(req_word)
		html = req.text
		asoup = soup(html, 'html.parser')
		
		#현재가
		ret_price = 0
		pdiv = asoup.find_all("div",{"class","new_totalinfo"})
		for ppdiv in pdiv:
			 for adl in ppdiv.find_all("dl"):
			 	for add in adl.find_all("dd"):
			 		try:
			 			if "현재가" in add.text:
			 				ret_price = int(su.sUtil.make_digit(add.text.split()[1]))			 			
			 		except ValueError: pass
		return ret_price

	#다음에서 전종목을 긁어와서 메모리에 삽입함
	def getCode(self):
		self.word_code = []
		self.word_codes = []
		req = requests.get('http://finance.daum.net/quote/all.daum?type=S&stype=P')
		req_ex = requests.get('http://finance.daum.net/quote/all.daum?type=U&stype=Q')
		html = req.text
		html_ex = req_ex.text
		asoup = soup(html, 'html.parser')
		asoup2 = soup(html_ex, 'html.parser')
		timestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime("%Y%m%d"))

		#코스피
		a1 = asoup.find_all("table",{"class","gTable clr"})
		for table in a1:
			for td in table.find_all("td",{"class","txt"}):
				self.word_code.append(td.text.replace(";",""))
				for at in td.find_all("a"):
					atag = at.attrs['href']
					self.word_code.append(atag[atag.find('=')+1:])
					self.word_code.append("KOSPI")
					self.word_code.append(timestamp)
					#기초 필터링
					if self.check_filter_word(self.word_code[0]) :
						self.word_code = []
					else:
						self.word_codes.append(self.word_code)
						self.word_code = []

		#코스닥
		a2 = asoup2.find_all("table",{"class","gTable clr"})
		for table in a2:
			for td in table.find_all("td",{"class","txt"}):
				self.word_code.append(td.text.replace(";",""))
				for at in td.find_all("a"):
					atag = at.attrs['href']
					self.word_code.append(atag[atag.find('=')+1:])
					self.word_code.append("KOSDAQ")
					self.word_code.append(timestamp)
					#기초 필터링
					if self.check_filter_word(self.word_code[0]) :
						self.word_code = []
					else:
						self.word_codes.append(self.word_code)
						self.word_code = []

		#듀플중복삭제
		self.ret_codes = []
		for aw in self.word_codes:
			if aw not in self.ret_codes:
				self.ret_codes.append(aw)
				
		return self.ret_codes

	def divide_list(self,alist,reg):
		ret_list,append_list,rev_list = [],[],[]
		while alist: rev_list.append(alist.pop())
		idx = 0
		while rev_list:
			append_list.append(rev_list.pop())
			idx += 1
			if idx==reg or len(rev_list) == 0:
				idx = 0
				ret_list.append(append_list)
				append_list = []
		return ret_list

	#전체코드를 불러와서 회사 기본사항을 넣는다.
	def update_2ndFilter(self):
		codelist = self.m_dbm.getCodeFromDB('all')
		#200개씩 분할해서 작업
		devide_lists =  self.divide_list(codelist,200)
		for apart in devide_lists:
			self.m_webFileter.update_2ndFilter(apart)
			#네이버에서 접속이 빈번하게 일어나면 나를 자르는듯
			time.sleep(3)

	#흑자기업을 대상으로 재무재표를 다시 검색한다.  (네이버에서..)
	def update_3rdFilter(self):
		codelist = self.m_dbm.getCodeFromDB('benefit')
		devide_lists =  self.divide_list(codelist,200)
		for apart in devide_lists:
			self.m_webFileter.update_3rdFilter(apart)			
			time.sleep(3)

	#재무재표에 이상이 없으고 점수가 높으면
	#지난 12개월치 데이터를 받아서 작업한다.
	def update_4thFilter(self,time_flag):
		codelist = self.m_dbm.getCodeFromDB('pass_finance')
		print('%d 개의 재무정보로 4차필터 진행합니다.'%(len(codelist,)))
		devide_lists = self.divide_list(codelist,50)
		for apart in devide_lists:
			self.m_chartFilter.update_4thFilter(apart,time_flag)
			time.sleep(5)

		#act 테이블에 종료시간을 넣는다.
		self.m_dbm.update_act(time_flag)

	#최종적으로 기관과 외국인 수급을 검사한다.
	def update_verifyFilter(self):
		codelist = self.m_dbm.getCodeFromDB('step4')
		#코드로 받은것중에서 갭상은 제외한다.
		#codelist = []	for crow in codes:	if not crow[14] == "2갭상주":	codelist.append(crow)
		print('4차필터링된 종목들로 기관과 외국인 수급을 검사합니다.')
		self.m_verifyFilter.chk_foreign_income(codelist)		

	#추천주가 지난3년대비 40%이상이면 제외한다.
	def chk_low_price_3years(self,time_flag):
		codelist = self.m_dbm.getCodeFromDB('step4')		
		self.m_verifyFilter.chk_low_price_3years(codelist,time_flag)

if __name__ == '__main__':
	gtr = CodeGetter()		
	#gtr.m_dbm.codeToDB(gtr.getCode())	#오늘날자로 주식시장에 등록되어있는 종목을 넣는다.
	#gtr.update_2ndFilter()	#회사기본정보 삽입및 per필터(per필터는 일단 보류해봐야겠다..)
	#gtr.update_3rdFilter()	#회사제무정보로 점수 갱신
	#gtr.update_4thFilter(1)
	#gtr.update_verifyFilter()	#기관이나 외인국 수급을 검사한다.
	gtr.chk_low_price_3years(1)  #지난삼년 대비 현재가의 위치 