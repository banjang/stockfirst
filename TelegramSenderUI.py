from threading import Thread
from dbmanager import dbmanage as dbm

class TelegramUI(Thread):
    width, height = 450, 260

    def __init__(self):
        Thread.__init__(self)
        self.m_dbm = dbm()
        self.start()

    def run(self):
        pass


if __name__ == '__main__':
    mainUI = TelegramUI()
