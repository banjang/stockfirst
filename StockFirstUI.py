from tkinter import *
from tkinter import font,filedialog,messagebox
from threading import Thread
from queue import Queue
import TelegramSender as req
import random,string,time,os

#내부메시지 피커
class MessagePicker(Thread):
    def __init__(self, LWid, q):
        Thread.__init__(self)
        self.ListWid = LWid
        self.q = q
        self.EndFlag = False

    def run(self):
        while self.EndFlag != True:
            if self.q.qsize() > 0:
                self.ListWid.insert(0, str(self.q.get()))
            time.sleep(0.05)

    def exitThread(self):
        print("Picker Thread 종료합니다.")
        self.EndFlag = True

#UI클래스
class StockFrame(Thread):
    width, height = 550, 160

    def __init__(self,amother):
        Thread.__init__(self)
        self.mother = amother
        self.start()

    def run(self):
        self.root = Tk()
        self.root.title("FirstStock")
        ###
        self.myFont = font.Font(family='Consolas', size=11)
        self.mySFont = font.Font(family='Consolas', size=9)

        # 리사이즈금지
        self.root.resizable(0, 0)

        # 종료시퀀스
        self.root.protocol("WM_DELETE_WINDOW", self.OnExit)

        #
        self.W1Frame = Frame(self.root, width=int(StockFrame.width * 0.8), height=StockFrame.height)
        self.W1Frame.pack_propagate(0)
        self.W1Frame.pack(side=LEFT, fill=BOTH)
        self.W1List = Listbox(self.W1Frame, width=int(StockFrame.width * 0.8), height=StockFrame.height,
                              font=self.myFont)
        self.W1List.pack(side=TOP)

        #
        self.W2Frame = Frame(self.root, width=int(StockFrame.width * 0.2), height=int(StockFrame.height))
        self.W2Frame.pack_propagate(0)
        self.W2Frame.pack(side=TOP, fill=BOTH)
        self.W2Btn = Button(self.W2Frame, text='Start', command=self.OnStart, width=int(StockFrame.width * 0.2)
                            , height=int(StockFrame.height), font=self.myFont)
        self.W2Btn.pack(side=TOP)

        # make queue & messagePicker
        self.inQueue = Queue()
        self.inMessagePicker = MessagePicker(self.W1List, self.inQueue)
        self.inMessagePicker.start()

        #start UI loop
        self.root.mainloop()

    def OnStart(self):
        self.inQueue.put(str(time.time()))
        #for _ in range(500):
            #self.W1List.insert(0,str(self.W1List.size())+" : "+''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10)))
            #time.sleep(0.05)

    def OnExit(self):
        print("종료합니다.")
        self.inMessagePicker.exitThread()
        self.root.quit()
        self.mother.EndMain()

if __name__ == '__main__':
    mainUI = StockFrame(None)
