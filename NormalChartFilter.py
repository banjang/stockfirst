import dbmanager as dbm
#import matplotlib.pyplot as plt

class normalchartfilter:
	def __init__(self):
		self.m_dbm = dbm.dbmanage()

	#급등주를 통과하지 못한아이들이다.
	#재검사를 한다. 양봉이면서 그물망을 벗어나려한다. 
	#그리고 거래량과 3%이상 안보고나서 나머지 지표들이 전부 웃어주면 추천한다.
	#저점에 발생하는 두번째 갭상 양봉.. 도.. 있어야한다.??
	#앞으로 주가가상승할것이라는 신호를 보내면 시장엔 반드시 호재가 출현한다.
	def is_other_ok(self,acorp,alist):		
		#스토케스틱 잘못됬음  80이하로 잡고	#골든크로스만 보자 
		#리테스팅은 테스트 후에 하는걸로 하자
		if self.is_normal_stockcastic(alist) == False: 
			print('기술주 스톡케스틱 시그널 아님')
			return

		#MACD가 2일연송 상승 했거나 MACD가 오늘 골든 크로스이거나		
		#그리고 오늘 RATE가 -1.5 와 1.5을 벗어났으면 안된다.
		if self.is_normal_macd(alist) == False:
			print('기술주 MACD 시그널 아님')
			return

		#RSI 80아래 40보다 위어야한다.
		if self.is_normal_rsi(alist) == False: 
			print('기술주 RSI 시그널 아님')
			return

		#양봉검사 
		if float(alist[-1][4]) <= float(alist[-1][1]):
			print('양봉아님')
			return

		#이평선 정배열이 없다!! 20일선?

		print('기술주로 편입됨')
		#그물망검사 (보류중)
		#if self.is_normal_net(alist) == False: 
		#	print('기술주 그물망 못벗어남')
		#	return

		score = 45
		self.m_dbm.update_normal_corp_score(acorp[1],score)
		return True

	#저점에서 발생하는 2번째 갭상양봉인가..
	def is_2nd_red(self,acorp,alist):		
		#만들어진 get_list의 구성 : #리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]	
		end_prices = []
		cf1 = float(alist[-1][4]) > float(alist[-1][1])	  #마지막날 양봉
		cf2 = float(alist[-1][1]) > float(alist[-2][4])   #마지막날은 갭상 양봉
		cf3 = float(alist[-2][4]) > float(alist[-2][1])   #1전날은 양봉
		cf4 = float(alist[-3][4]) < float(alist[-3][1])   #2전날은 음봉	
		if not (cf1 and cf2 and cf3 and cf4): return False
		for row in alist: end_prices.append(float(row[4]))
		#마지막날 시가가 52주종가 중간레벨보다 위쪽에서 발생한것이라면 False
		if float(alist[-1][1]) > ((min(end_prices) + max(end_prices)) / 2): return False

		score = 45
		self.m_dbm.update_2gap_corp_score(acorp[1],score)
		return True

###############################
	#이동평균을 구해서 돌려줌
	def make_avr(self,alist,av_number):
		ret_list = []
		for idx,row in enumerate(alist):
			suma = 0.0
			if idx < (av_number-1): 
				for bef in range(idx+1): suma += alist[bef]
				ret_list.append(suma / (idx+1))
				continue
			for af in alist[idx-(av_number-1):idx+1]: suma += af
			ret_list.append(suma / av_number)
		return ret_list

	#그물망 (보류됨)
	def is_normal_net(self,alist):
		end_prices = []
		for idd,dayrow in enumerate(alist): end_prices.append(float(dayrow[4]))
		tot_length = len(end_prices)

		#기간 5 간격 12
		avrlist = []
		perid,term = 5,12
		for tr in range(term): avrlist.append(self.make_avr(end_prices,((tr+1)*perid)))		

		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
		#마지막 날이 양봉인가
		if not (float(alist[-1][4]) > float(alist[-1][1])): 
			return False

		#그물망에 걸쳐있거나 벗어나려하는가?
		last_value = float(alist[-1][4])
		last_volume = float(alist[-1][5])
		net_count = 0
		for row in avrlist:
			if  float(last_value) <= float(row[tot_length-1]):
				net_count += 1
		if net_count > 3: 
			return False		

		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량] alist
		#여기부터 중요함
		#과거 15일 보면서 3프로 이상의 양봉이 있어야하고 그양봉이 지난 5일을 덮었어야하고
		#그양봉의 거래량이 평소거래량 대비 300%여야하고  
		#그리고 금일 양봉의 시초가가 장대양봉의 시가보다 낮으면 안됨..
		#평소거래량 300% 인것은..  과거 5일을 보되 과거 5일이에 300% 이상인것은 4일이상이어야한다. 하루는 봐줄께.
		back_days = 15
		pdays = 5
		big_ex = False
		for dy in range(back_days):
			tday = -1 - (dy+1)
			#양봉판단
			if float(alist[tday][1]) >= float(alist[tday][4]): continue
			#3프로판단
			if float(alist[tday][4]) / float(alist[tday-1][4])  < 1.3: continue
			#5일덮기판단
			all_up = True
			for upp in range(pdays):
				if not float(alist[tday][4]) > max(float(alist[tday-(upp+1)][1]),float(alist[tday-(upp+1)][4])):
					all_up = False
					break
			if all_up == False : continue
			#5일평소거래량대비 300% 판단
			up3_days = 0
			for up3 in range(pdays):
				if float(alist[tday][5]) >= (float(alist[tday-(up3+1)][5]) * 3):
					up3_days += 1				
			if up3_days < 4 : continue

			#모두통과이고 마지막날시가가 타겟일의 시가보다 크면 멈춘다
			if all_up == True and up3_days >= 4 and float(alist[-1][1]) > float(alist[tday][1]):
				big_ex = True
				break

		if big_ex == True : return True
		else : return False
		#현재가가 높은지 낮은지는 뒤에서 판단한다.

	#MACD가 상승 했거나 #MACD가 오늘 골든 크로스이거나	
	#그리고 오늘 RATE가 -1.5 와 1.5을 벗어났으면 안된다.
	def is_normal_macd(self,alist):
		if len(alist) < 1 : return
		avr12, avr26, macd, signal, macdrate = [],[],[],[],[]
		EP12 = float(2 / (12+1))
		EP26 = float(2 / (26+1))
		SIGEP9 = float(2 / (9+1))

		#12지수평균 26지수평균 MACD와 SIGNAL 값을 삽입한다.
		for idx,row in enumerate(alist):
			if idx == 0:	#첫날엔 지수이동평균가가 종가이다.
				avr12.append(float(alist[idx][4]))
				avr26.append(float(alist[idx][4]))
				macd.append(0.0)
				signal.append(0.0)
				macdrate.append(0.0)
				continue

			avr12.append(float(alist[idx][4])*EP12 + avr12[idx-1]*(1-EP12))
			avr26.append(float(alist[idx][4])*EP26 + avr26[idx-1]*(1-EP26))
			macd.append(avr12[idx] - avr26[idx])
			signal.append(float(macd[idx])*SIGEP9 + float(signal[idx-1])*(1-SIGEP9))
			macdrate.append(float(macd[idx] / float(alist[idx][4]) * 100))

		af,sg,ar = [],[],[]
		af.append(macd[len(macd)-3])
		af.append(macd[len(macd)-2])
		af.append(macd[len(macd)-1])
		
		sg.append(signal[len(signal)-3])
		sg.append(signal[len(signal)-2])
		sg.append(signal[len(signal)-1])
		
		ar.append(macdrate[len(macdrate)-1])
						
		ret_flag = False
		#MACD가 상승 하면 True
		if af[1] < af[2]:
			ret_flag = True

		#MACD 시그널이 상승 하면 True
		if sg[1] < sg[2]:
			ret_flag = True

		#MACD가 오늘 골든크로스면 True
		if sg[2] < af[2] and af[1] <= af[2] and sg[1] >= af[1]: 
			ret_flag = True

		#오늘 MACDrate가 -1.5~1.5을 벗어나면 안된다.
		if not (ar[0] >= -1.5 and ar[0] <= 1.5):	
			ret_flag = False

		return ret_flag

	#이거 특이한 스토케스틱이다 5 3 3 은 굉장히 특이하다 미리 알려주는것이다. 따라서 당일 발생한거는 신뢰도가 낮다
	#스토케스틱은 오늘 k가 d보다 위에 있어야 하고 90아래쪽이어야한다.
	#뒤에서 장대양봉검사를한다. #리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
	def is_normal_stockcastic(self,alist):
		#print(alist)	
		#평활계수 EP  = 2 / (n + 1)
		#kp = (금일종가 - 5일중 최저가) / (5일중최고가 - 5일중최저가) * 100
		#슬로우 스토캐스틱 sk = (kp값 * EP) + 직전sk값 * (1 - EP)
		#슬로우 스토캐스틱 sd = (sk값 * EP) + 직전sd값 * (1 - EP)
		end_prices,min_prices,max_prices = [],[],[]		
		for dayrow in alist: 
			max_prices.append(float(dayrow[2]))		
			min_prices.append(float(dayrow[3]))
			end_prices.append(float(dayrow[4]))
		##
		M,N1,N2 = 5,3,3
		fk,fd,sd = [],[],[]
		EP = 2 / (N1 + 1)
		for idx,ep in enumerate(end_prices):
			if idx < (M-1) :
				fk.append(0.0)
				fd.append(0.0)			
				sd.append(0.0)
				continue
			
			fk_top = (end_prices[idx] - min(min_prices[idx-(M-1):idx+1]))
			fk_base = (max(max_prices[idx-(M-1):idx+1]) - min(min_prices[idx-(M-1):idx+1]))				
			if fk_base == 0 : fk.append(100.0)		
			else:
				pfk = fk_top / fk_base * 100 
				fk.append(pfk)

			pfd = (pfk * EP) + fd[idx-1] * (1 - EP)
			fd.append(pfd)
			psd = (pfd * EP) + sd[idx-1] * (1 - EP)
			sd.append(psd)

		#마지막 2일의 지표가 상승이거나 마지막날에 골든 크로스가 나면된다.
		ret_flag = False
		lk,ld = [],[]
		lk.append(fd[len(fd)-3])
		lk.append(fd[len(fd)-2])
		lk.append(fd[len(fd)-1])
		
		ld.append(sd[len(sd)-3])
		ld.append(sd[len(sd)-2])
		ld.append(sd[len(sd)-1])
		
		#마지막날이 80 안쪽이어야한다.  (삭제됨)
		#if float(lk[1]) > 80.0 : return False

		#2일연속 지표상승했다면 True  (삭제됨)
		#if lk[0] < lk[1] and lk[1] < lk[2]: ret_flag = True

		#마지막날에 골든크로스를 검사
		if ret_flag == False:				
			if ld[2] < lk[2] and lk[1] < lk[2] and ld[1] >= lk[1]:
				ret_flag = True

		#리테스팅은 d선위에서 k가줄었다가 상승하는 경우다. (추후편입예정)
		'''
		if ret_flag == False:
			if lk[0] > ld[0] and lk[1] > ld[1] and lk[2] > ld[2]:
				g0 = lk[0] - ld[0]
				g1 = lk[1] - ld[1]
				g2 = lk[2] - ld[2]
				if g0 > g1 and g2 > g1:
					ret_flag = True		
		'''
		return ret_flag

	#RSI 오늘 상승했어야하고  80아래 40보다 위어야한다.
	def is_normal_rsi(self,alist):
		end_prices = []
		for dayrow in alist: end_prices.append(float(dayrow[4]))		
		rs = 14
		rsi = []		
		for idx,edp in enumerate(end_prices):			
			if idx < rs:	#15개가 있어야 14일이동평균이 나온다.
				rsi.append(0.0)
				continue
			#14부터 시작한다.
			add,sub = 0,0
			parea = end_prices[idx-14:idx+1]
			for ix,val in enumerate(parea):
				if ix == 0: continue
				gap = parea[ix] - parea[ix-1]
				if gap >= 0: add += gap
				else: sub += abs(gap)
			if (add + sub) == 0 : rsi.append(0.0)
			else : rsi.append( add / (add+sub) * 100)

		
		####
		ret_flag = False
		lr = []
		lr.append(rsi[len(rsi)-2])
		lr.append(rsi[len(rsi)-1])

		#
		if lr[1] < 81 and lr[1] > 39:
			ret_flag = True

		#우상향은 매수세가 있다는걸 의미한다
		#매수폭 / (매도폭 + 매수폭) 이므로
		if lr[0] > lr[1]:
			ret_flag = False

		#RSI가 40을 돌파하면 급등이 나올것 같으므로
		return ret_flag


if __name__ == '__main__':
	print('it is normal chart')
