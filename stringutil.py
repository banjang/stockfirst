#문자유틸
class sUtil:
	@classmethod
	def make_dottofloat(self,dotstring):
		#0.13가 .13 이런식으로 온다.
		if dotstring.index('.') == 0: return ('0'+dotstring)
		else: return dotstring

	@classmethod
	def removecomma_list(self,alist):
		ret_list = []
		for row in alist: ret_list.append(row.replace(',',''))
		return ret_list

	@classmethod
	def make_digit(self,snumber):
		if len(snumber) < 1:
			return snumber
		ret_words = []
		for aw in snumber:
			if aw.isdigit():
				ret_words.append(aw)
			if aw == '-':
				ret_words.append(aw)
			if aw == '.':
				ret_words.append(aw)
		return ''.join(ret_words)


	@classmethod
	def make_number(self,snumber):
		if len(snumber)==1 and snumber=='-':
			return 0
		else:
			return float(sUtil.make_digit(snumber))
		#if float(Tmp) - int(Tmp) != 0.0 : return flot(Tmp)
		#else : return int(Tmp)

	@classmethod
	def is_only_digit(self,aword):		
		for aw in aword:
			if not aw.isdigit():
				return False
		return True

	@classmethod
	def is_digit(self,aword):
		if len(aword) < 1: return False
		ret_flag = True
		idx = 0
		for aw in aword:
			idx += 1
			if aw.isdigit() or aw == ',' or aw == '.': continue
			if aw == '-' and idx == 1: continue
			ret_flag = False
		return ret_flag

if __name__ == '__main__':
	print(sUtil.get_today_word())
	'''
	Tmp = '-28fy802hfhf.0f280hf3%#@'
	print(type(sUtil.make_digit(Tmp)))
	print(sUtil.make_digit(Tmp))

	Tmp = '345'
	print(type(sUtil.make_number(Tmp)))
	print(sUtil.make_number(Tmp))

	Tmp ='-'
	print(type(sUtil.make_number(Tmp)))
	print(sUtil.make_number(Tmp))
	'''