import sqlite3
from Schedule import timeUtil

class dbmanage:
	def __init__(self):
		pass

	#모든 테이블이 있는지 검사후 없으면 만들기
	def crate_all_table(self):
		tb_names = {'tb_codes':self.create_tb_codes,'tb_rcm':self.create_tb_rcm,'tb_act':self.create_tb_act}
		for tn in tb_names.keys():
			if self.check_table_exist(tn) == False:
				tb_names[tn]()
		return self

	#step4까지간 추천주중에서 현재가격이 너무 높다면 '고가기술주' 라고 붙힌다.
	def update_manage_text_too_high(self,acode):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		addt = '고가 '
		print('회사번호(%s) 고가격주로 분류됨'%(acode,))
		curr.execute("SELECT manage_text FROM tb_codes WHERE codenumber==(?) and filter_step==4",(str(acode),))
		for arow in curr.fetchall(): 
			addt += str(arow[0])
		curr.execute("UPDATE tb_codes SET manage_text=(?) WHERE codenumber==(?)",(addt,acode))
		conn.commit()
		curr.close()
		conn.close()		

	#step4까지간 추천주중에서 기관과 외국인 수급이 없다면 미수급주
	def update_step_down_not_imcome(self,acode):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		addt = '미수급 '
		print('회사번호(%s) 미수급주로 분류됨'%(acode,))
		curr.execute("SELECT manage_text FROM tb_codes WHERE codenumber==(?) and filter_step==4",(str(acode),))
		for arow in curr.fetchall(): 
			addt += str(arow[0])			
		curr.execute("UPDATE tb_codes SET manage_text=(?) WHERE codenumber==(?)",(addt,acode))
		conn.commit()
		curr.close()
		conn.close()


	#텔레그램 주식로봇이 글자로조회한경우
	def get_corp_names(self,astr):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()		
		curr.execute("SELECT * FROM tb_codes WHERE codename like ('%' || ? || '%') ",(astr,))
		ret_list = []		
		for row in curr.fetchall():
			ret_list.append(row)
		curr.close()
		conn.close()
		return ret_list	

	#텔레그램 주식로봇이 숫자(코드번호)로 조회한경우
	def get_corp_codes(self,astr):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()		
		curr.execute("SELECT * FROM tb_codes WHERE codenumber == (?)",(astr,))
		ret_list = []		
		for row in curr.fetchall():
			ret_list.append(row)
		curr.close()
		conn.close()
		return ret_list

	#거래량 미달로 점수를 10점으로 초기화한다
	def update_corp_volume_score(self,list_score):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		print('회사번호(%s)거래량 미달.'%(list_score[0],))
		curr.execute("UPDATE tb_codes SET invest_score=10, manage_text=(?) WHERE codenumber==(?)"
			,('거래량미달',list_score[0]))
		conn.commit()
		curr.close()
		conn.close()

	#여러회사를 한번에 업데이트
	def add_corp_score(self,alist):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		for row in alist:
			print('회사번호(%s)에  점수(%s)를 가산합니다.'%(str(row[0]),str(row[1])) )
			score = 0
			curr.execute("SELECT invest_score FROM tb_codes WHERE codenumber==(?)",(str(row[0]),))
			for arow in curr.fetchall(): score += int(arow[0])
			score += int(row[1])
			curr.execute("UPDATE tb_codes SET invest_score=(?),filter_step=(?) WHERE codenumber==(?) and filter_step==2"
			,(score,3,row[0]))
		conn.commit()
		curr.close()
		conn.close()

	#2번째 갭상양봉의경우 manage_text에  2갭상주 이라고 적어넣는다.
	def update_2gap_corp_score(self,acorp,ascore):
		#manage_text에  2갭상주 이라고 적어넣는다.
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		score = 0
		print('회사번호(%s) 점수(%s) 가산함.'%(acorp,ascore))
		curr.execute("SELECT invest_score FROM tb_codes WHERE codenumber==(?) and filter_step==3",(str(acorp),))
		for arow in curr.fetchall(): score += int(arow[0])
		if score == 0: return #아무것도 조회되지 않음
		score += int(ascore)
		curr.execute('UPDATE tb_codes SET invest_score=(?),filter_step=4, manage_TEXT="2갭상주" WHERE codenumber==(?)',(score,str(acorp)))
		conn.commit()
		curr.close()
		conn.close()

	#일반주이지만 나머지 지표들이 전부웃어주고 있다. 라면 추천
	def update_normal_corp_score(self,acorp,ascore):
		#manage_text에 기술분석 추천주라고 쓴다.
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		score = 0
		print('회사번호(%s) 점수(%s) 가산함.'%(acorp,ascore))
		curr.execute("SELECT invest_score FROM tb_codes WHERE codenumber==(?) and filter_step==3",(str(acorp),))
		for arow in curr.fetchall(): score += int(arow[0])
		if score == 0: return #아무것도 조회되지 않음
		score += int(ascore)
		curr.execute('UPDATE tb_codes SET invest_score=(?),filter_step=4, manage_TEXT="기술주" WHERE codenumber==(?)',(score,str(acorp)))
		conn.commit()
		curr.close()
		conn.close()

	#해당회사를 해당점수를 가산 UPDATE한다.
	#급등주로써 추천되어진 종목이기 때문에 manage_text에 '급등주'라고 적는다.
	def update_corp_score(self,acorp,ascore):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		score = 0
		print('회사번호(%s) 점수(%s) 가산함.'%(acorp,ascore))
		curr.execute("SELECT invest_score FROM tb_codes WHERE codenumber==(?) and filter_step==3",(str(acorp),))
		for arow in curr.fetchall(): score += int(arow[0])
		if score == 0: return #아무것도 조회되지 않음
		score += int(ascore)
		curr.execute('UPDATE tb_codes SET invest_score=(?),filter_step=4, manage_TEXT="급등주" WHERE codenumber==(?)',(score,str(acorp)))
		conn.commit()
		curr.close()
		conn.close()

	#해당이름의 테이블이 있는지 검사
	def check_table_exist(self,tbName):
		compare_word = ''
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute("SELECT name FROM sqlite_master WHERE type='table' AND name=(?)",(tbName,))
		for row in curr.fetchall():
			compare_word = str(row[0])
		curr.close()
		conn.close()
		if compare_word == tbName: return True
		else: return False

	#tb_codes 만들기
	def create_tb_codes(self):		
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute('CREATE TABLE IF NOT EXISTS tb_codes(codename TEXT,codenumber TEXT,iskospi TEXT,timestp INT,invest_score INT,bday_price INT,bday_volume INT,max52 INT,min52 INT,totvalue INT,stock_count INT,per TEXT,eps TEXT,filter_step INT,manage_text TEXT)')
		conn.commit()
		curr.close()
		conn.close()

	#tb_codes 클린작업
	def cleanDB(self):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute("DELETE FROM tb_codes WHERE timestp > 0")
		conn.commit()
		curr.close()
		conn.close()

	def codeToDB(self,a_codes):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		#갱신당일(오늘)이 아닌것은 삭제
		self.cleanDB()
		#전종목 삽입
		code_counter = 0
		for aw in a_codes:
			curr.execute("INSERT INTO tb_codes (codename,codenumber, iskospi, timestp) VALUES(?,?,?,?)",(aw[0],aw[1],aw[2],int(aw[3])))
			code_counter+=1
		conn.commit()
	    #종료
		curr.close()
		conn.close()
		print("%d개의 종목이 DB로 삽입이되었습니다."%(code_counter))	

	#tb_codes 에서 해당 컨디션에 해당하는 종목리스트를 가져온다.
	def getCodeFromDB(self,condition):
		ret_list = []
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()

		if "all" == condition:
			curr.execute("SELECT * FROM tb_codes")
			for row in curr.fetchall():	ret_list.append(list(row))
		elif "benefit" == condition:
			curr.execute("SELECT * FROM tb_codes WHERE invest_score > 0")
			for row in curr.fetchall():	ret_list.append(list(row))
		elif "pass_finance" == condition:	#재무필터인데 26점이상은 최근에 적자는 나지 않은상황이다.
			curr.execute("SELECT * FROM tb_codes WHERE invest_score > 38")
			for row in curr.fetchall(): ret_list.append(list(row))
		elif "step4" == condition:
			curr.execute("SELECT * FROM tb_codes WHERE filter_step == 4")
			for row in curr.fetchall(): ret_list.append(list(row))

		curr.close()
		conn.close()
		return ret_list
		
	#추천종목 tb_rcm 테이블 만들기
	def create_tb_rcm(self):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute('CREATE TABLE IF NOT EXISTS tb_rcm(id integer primary key autoincrement,codename TEXT,codenumber TEXT,rcm_time TEXT,rcm_step INT,invest_score INT,rcm_text TEXT)')
		conn.commit()
		curr.close()
		conn.close()

	#step4 까지온 종목들을 추천테이블로 이동시킨다.
	def moveto_rcm(self,time_flag):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		nowtime = timeUtil.get_today_word()
		curr.execute("SELECT * FROM tb_codes WHERE filter_step == 4")
		#있는것들 name,code, rcm_time,rcm_step, invest_score,rcm_text
		ret_list = []
		get_field = [0,1,4,14]
		for row in curr.fetchall():
			tmp_list = []
			for idx in get_field:
				tmp_list.append(row[idx])
			ret_list.append(tmp_list)

		atoday = timeUtil.get_today_word()
		for rrow in ret_list:
			curr.execute("INSERT INTO tb_rcm VALUES(NULL,(?),(?),(?),(?),(?),(?))",(rrow[0],rrow[1],atoday,time_flag,rrow[2],rrow[3]))		
			#추천테이블로 옮겼으니 원래대로 step을 3으로 바꾸고 manage_text를 null로 바꾼다. 오후검색을 위해서..
			curr.execute("UPDATE tb_codes SET filter_step=3,manage_text='' WHERE codenumber == (?)",(rrow[1],))

		conn.commit()
		curr.close()
		conn.close()

	#추천테이블에서 긁어와서 list로 반환
	def select_tb_rcm(self,time_flag):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		atoday = timeUtil.get_today_word()
		curr.execute("SELECT * FROM tb_rcm WHERE rcm_time=(?) and rcm_step=(?)",(atoday,time_flag))
		ret_list = []		
		for row in curr.fetchall():
			ret_list.append(row)
		curr.close()
		conn.close()
		return ret_list

	#tb_act 테이블 만들기
	def create_tb_act(self):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute('CREATE TABLE IF NOT EXISTS tb_act(day_stamp TEXT,step INT,start_time TEXT,end_time TEXT)')
		conn.commit()
		curr.close()
		conn.close()

	#act테이블 select
	def select_act(self):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		nowtime = timeUtil.get_today_word()
		curr.execute("SELECT * FROM tb_act WHERE day_stamp=(?) ORDER BY step desc",(nowtime,))
		ret_list = []		
		for row in curr.fetchall():
			ret_list.append(row)		
		curr.close()
		conn.close()
		return ret_list

	#삽입작업 날짜,오전오후,현제단계문자,시작시간
	def insert_act(self,ds,stp,stt):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		curr.execute("INSERT INTO tb_act (day_stamp,step,start_time) VALUES ((?),(?),(?))",(ds,stp,stt))
		conn.commit()
		curr.close()
		conn.close()

	#종료시간 업데이트
	def update_act(self,astp):
		conn = sqlite3.connect('stock.db')
		curr = conn.cursor()
		t_today = timeUtil.get_today_word()
		t_time = timeUtil.get_today_time_word()
		curr.execute('UPDATE tb_act SET end_time=(?) WHERE day_stamp == (?) and step == (?)',(t_time,t_today,astp))
		conn.commit()
		curr.close()
		conn.close()

if __name__ == '__main__':
	tdb = dbmanage()
	#tdb.insert_act(timeUtil.get_today_word(),2,timeUtil.get_today_time_word())
	print(tdb.getCodeFromDB('pass_finance'))
	
	