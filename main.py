import libchecker
from threading import Thread
import time,datetime
from webCodeList import CodeGetter
from dbmanager import dbmanage
import Schedule as sc
import TelegramBasic as tel
import sys,os

#타이머 클래스
class MainTimerThread(Thread):
    #초기멤버를 가지고 있는다..
    def __init__(self):
        Thread.__init__(self)
        self.delaySecount = 3*60        
        #self.m_telebot = tel.teleBot()
        self.m_timeChecker = sc.timeschedule().set_holidays()
        self.m_codeGetter = CodeGetter()
        self.m_dbm = dbmanage()
        self.EndFlag = False

    #실행부분.
    def run(self):
        while self.EndFlag != True:            
            self.do_action()
            time.sleep(self.delaySecount)

    #코드가 끝났으니 추천코드는 tb_rcm으로 이동하고 텔레그램으로 통보한다.
    def do_rcm(self,time_flag):
        #추천종목을 tb_rcm으로 이동한다.
        self.m_dbm.moveto_rcm(time_flag)
        #추천종목을 받아온다.
        rcm_lists = self.m_dbm.select_tb_rcm(time_flag)
        rcm_words,rcm_photo = [],[]        
        if len(rcm_lists) > 0 :
            #id,codename,codenumber,rcm_time,rcm_step,invest_score,rcm_text
            for row in rcm_lists:
                rcm_word = ""
                tt = datetime.datetime.now()
                nowprice = int(self.m_codeGetter.get_now_price(row[2]))
                rcm_word = "%s(%s)종목 점수(%s) 현재가(%s)원 %s로 추천종목 입니다. 매물대, 정치, DART상황 등을 고려하여 투자하세요."%(row[1],row[2],row[5],str(nowprice),row[6])
                if tt.weekday() == 4 :
                    rcm_word += '오늘은 금요일입니다. 투자에 주의가 필요합니다.'
                rcm_words.append(rcm_word)                                 
                rcm_photo.append('http://cichart.moneta.kr/pax/chart/candleChart/V201200/paxCandleChartV201200Weekly.jsp?abbrSymbol='+str(row[2]))

        #텔레그램으로 전송한다.
        for idx,req_w in enumerate(rcm_words):            
            self.m_telebot.sendPhoto_all(rcm_photo[idx],req_w)

    #시작할때 코드시작을 테이블에 알리는데 오늘 날짜에 이미 시작한 플레그라면 그만둔다.
    def prepair_to_go(self,on_flag):        
        act_lists = self.m_dbm.select_act()        
        if len(act_lists) < 1:            
            self.m_dbm.insert_act(sc.timeUtil.get_today_word(),on_flag,sc.timeUtil.get_today_time_word())
            return True
        else : #뭔가들어있는데 이미간거라면 return False            
            act_list = act_lists[0] #order by desc 되어있어서 높은숫자가 처음이다.
            if on_flag == 1:
                if (int(act_list[1]) == 1): return False #이미갔음
            elif on_flag == 2:
                if (int(act_list[1]) == 2): return False #이미갔음
                else: #오전반(1)만 간경우
                    self.m_dbm.insert_act(sc.timeUtil.get_today_word(),on_flag,sc.timeUtil.get_today_time_word())
                    return True        

    def do_action(self):
        #장시간 체크 do_check를 강화한다.
        #추천시간이 아니라면 리턴한다 1은 오전시작장 2는 오후마감장
        on_flag = self.m_timeChecker.do_check()
        if on_flag < 1:
            print(sc.timeUtil.get_today_time_word(),end='')
            print(' 현재 정규시간이 아님..')
            return
        elif on_flag == 3:  #3이면 재무 수집시간이다.   밤12시에서 밤12시30분사이 
            self.m_codeGetter.m_dbm.codeToDB(self.m_codeGetter.getCode())   #오늘날자로 주식시장에 등록되어있는 종목을 넣는다.
            self.m_codeGetter.update_2ndFilter() #회사기본정보 삽입및 per필터(per필터는 일단 보류해봐야겠다..)
            self.m_codeGetter.update_3rdFilter() #회사제무정보로 점수 갱신

        else:   #1이나 2를 반환하면 시작을 해야한다.
            #시작할때 코드시작을 테이블에 알리는데 오늘 날짜에 이미 시작한 플레그라면 그만둔다.
            if self.prepair_to_go(on_flag) == False : return            

            #코드를 시작하고            
            self.m_codeGetter.update_4thFilter(on_flag) #차트를 보면처 최종통보를 한다.
            self.m_codeGetter.update_verifyFilter() #기관과 외국인 수급을 검사한다.
            self.m_codeGetter.chk_low_price_3years(on_flag) #추천주가 3년대비 최저가 50%이하에 해당하지 않으면 짜름
            #코드가 끝났으니 추천코드는 tb_rcm으로 이동하고 텔레그램으로 통보한다.
            self.do_rcm(on_flag)


    def EndMain(self):
        self.EndFlag = True

    def do_Test(self,on_flag):
        self.m_codeGetter.m_dbm.codeToDB(self.m_codeGetter.getCode())   #오늘날자로 주식시장에 등록되어있는 종목을 넣는다.
        self.m_codeGetter.update_2ndFilter() #회사기본정보 삽입및 per필터(per필터는 일단 보류해봐야겠다..)
        self.m_codeGetter.update_3rdFilter() #회사제무정보로 점수 갱신
        self.m_codeGetter.update_4thFilter(on_flag) #차트를 보면처 최종통보를 한다.
        self.m_codeGetter.update_verifyFilter() #기관과 외국인 수급을 검사한다.
        self.m_codeGetter.chk_low_price_3years(on_flag) #추천주가 3년대비 최저가 50%이하에 해당하지 않으면 짜름
        #코드가 끝났으니 추천코드는 tb_rcm으로 이동하고 텔레그램으로 통보한다.
        #self.do_rcm(on_flag)


if __name__ == '__main__':    
    os.chdir(sys.path[0])
    mt = MainTimerThread()
    mt.do_Test(1)
    #mt.start()
    #mt.join()
