#타임 스케줄러 작업..
#공휴일 링크 : http://www.officeholidays.com/countries/south_korea/index.php
#다른해가 진입이 되었을 경우 어떻게 되는지 확인해야하고 12월20일 대통령선거일이 특별하게 변경되었으므로 별로처리해야함
import requests
from bs4 import BeautifulSoup as soup
import datetime

#시간과 관련되 유틸성 클래스
class timeUtil:
    month_words = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    calendar_list = {
        'Jan':'01','Feb':'02','Mar':'03','Apr':'04',
        'May':'05','Jun':'06','Jul':'07','Aug':'08',
        'Sep':'09','Oct':'10','Nov':'11','Dec':'12'}

    @classmethod
    def get_today_word(self):
        tt = datetime.datetime.now()
        ret_word = ''
        ret_word += str(tt.year)
        if len(str(tt.month)) == 1 : ret_word += '0' + str(tt.month)
        else: ret_word += str(tt.month)
        if len(str(tt.day))== 1 : ret_word += '0' + str(tt.day)
        else: ret_word += str(tt.day)
        return ret_word

    @classmethod
    def get_today_time_word(self):
        tt = datetime.datetime.now()
        ret_word = ''
        if len(str(tt.hour)) == 1 : ret_word += '0' + str(tt.hour)
        else: ret_word += str(tt.hour)
        if len(str(tt.minute)) == 1 : ret_word += '0' + str(tt.minute)
        else: ret_word += str(tt.minute)
        if len(str(tt.second))== 1 : ret_word += '0' + str(tt.second)
        else: ret_word += str(tt.second)
        return ret_word

    #Feb 6, 2017  -> 20170206
    @classmethod
    def engtodate(self,eng_word):
        if len(eng_word) < 1 : return
        tmp = eng_word.split()
        tmp[1] = tmp[1].replace(',','')
        if len(tmp[1]) == 1: tmp[1] = '0' + tmp[1]
        tmp[0] = timeUtil.calendar_list[tmp[0]]
        return tmp[2] + tmp[0] + tmp[1]

    #7-Jul-16
    @classmethod
    def make_ymd(self,time_word):
        if len(time_word) < 1: return
        if '-' not in time_word : return
        tmp = time_word.split('-')
        tmp[1] = tmp[1].replace(tmp[1],timeUtil.calendar_list[tmp[1]])
        if len(tmp[0]) == 1: tmp[0] = '0'+tmp[0]
        if len(tmp[2]) == 2: tmp[2] = '20' + tmp[2]
        return tmp[2]+tmp[1]+tmp[0]

    @classmethod
    def dateStamp_comma(self,yy,mm,dd):
        if len(str(mm)) == 1 : mm = '0' + str(mm)
        if len(str(dd)) == 1 : dd = '0' + str(dd)
        return (str(yy)+','+str(mm)+','+str(dd))

    @classmethod
    def getDateStamp(self):
        now = datetime.datetime.now()
        ret = str(now.year)
        if len(str(now.month)) < 2: ret += '0' + str(now.month)
        else : ret += str(now.month)        
        if len(str(now.day)) < 2: ret += '0' + str(now.day)
        else : ret += str(now.day)
        return ret

#시간체크하는 클레스
class timeschedule:
    def __init__(self):
        self.calendar_list = {
        'Jan':'01','Feb':'02','Mar':'03','Apr':'04',
        'May':'05','Jun':'06','Jul':'07','Aug':'08',
        'Sep':'09','Oct':'10','Nov':'11','Dec':'12'}
        self.holidays_list = []
        #시간체크순서(토일체크 -> 공휴일체크 -> 장시간체크)
        self.timecheck_list = []
        self.timecheck_list.append(self.istoday_sday)
        self.timecheck_list.append(self.istoday_holiday)
        #self.timecheck_list.append(self.is_not_ontime)

    #do_check를 변경 할필요가 있다.
    #검사시간이 존재한다.  1차(8:00~9:00) 2차(13:10~14:10)
    def do_check(self):
        #토일요일이나 공휴일은 제외한다.
        for fnk in self.timecheck_list:
            if fnk() is True:        
                print('공휴일임.')
                return 0
        #추천시간대인가를 검사한다.
        a_rcm = self.is_rcm_time()
        if a_rcm > 0: return a_rcm
        else: return 0        

    #추천시간대인가 검사한다.
    def is_rcm_time(self):
        now = datetime.datetime.now()
        gaz = now.hour * 100 + now.minute
        if gaz >= 830 and gaz <= 855:   #어제자료로 오늘장 분석
            return 1
        elif gaz >= 1420 and gaz <= 1450:   #금일 중간자료로 분석
            return 2
        elif gaz >= 10 and gaz < 40:     #밤12시부터 재무재표 분석시간.
            return 3
        else: return 0

    def set_holidays(self):
        req = requests.get('http://www.officeholidays.com/countries/south_korea/index.php')
        html = req.text
        asoup = soup(html, 'html.parser')
        tb_soup = asoup.find_all("span",{"class","mobile_ad"})
        for t_span in tb_soup:
            pan = t_span.text
            repan = pan.replace(pan[0:3],self.calendar_list.get(pan[0:3])).replace(' ','')        
            if 3 == len(repan):
                self.holidays_list.append(repan[0:2] + '0' + repan[2:3])            
            elif 4 == len(repan):
                self.holidays_list.append(repan)
        return self

    def istoday_sday(self):
        now = datetime.datetime.now()
        n_year = now.year
        n_month = now.month
        n_date = now.day
        if n_month == 1 or n_month == 2:
            n_year -=1
            n_month = 12 + n_month
        a_year = int(str(n_year)[0:2])
        b_year = int(str(n_year)[2:4])
        res = int(21*a_year/4) + int(5*b_year/4) + int(26*(n_month+1)/10) + n_date -1
        days = res % 7

        if days == 0 or days == 6:
            return True
        else:
            return False

    def istoday_holiday(self):
        now = datetime.datetime.now()
        s_month = str(now.month)
        s_date = str(now.day)
        if 1 == len(s_month): s_month = '0' + s_month
        if 1 == len(s_date): s_date = '0' + s_date
        try:
            if self.holidays_list.index(s_month + s_date) >= 0 : return True
        except ValueError:
            return False

    def is_not_ontime(self):
        now = datetime.datetime.now()
        gaz = now.hour * 100 + now.minute
        if gaz >= 900 and gaz <= 1530:
            return False
        else:
            return True

if __name__ == '__main__':
    #print("스케쥴 테스트")
    #ts = timeschedule().set_holidays()
    #ts.do_check()
    #print(timeUtil.engtodate('Feb 6, 2017'))
    #ts = timeschedule()
    #print(ts.istoday_sday())
    #print(ts.istoday_holiday())
    print(timeUtil.get_today_time_word())