import libchecker
from threading import Thread
import telepot,requests
import time,json
from bs4 import BeautifulSoup as soup
from stringutil import sUtil
from dbmanager import dbmanage

class teleBot(Thread):
	def __init__(self):
		Thread.__init__(self)		
		with open('load_id.json') as data_file:
			self.users_data = json.load(data_file)
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			self.users_data[id_str]['inst'] = telepot.Bot(self.users_data[id_str]['token'])
			aTmp = self.users_data[id_str]['inst'].getUpdates()
			if len(aTmp) == 0 : self.users_data[id_str]['offset'] = 0
			else: self.users_data[id_str]['offset'] = int(aTmp[-1]['update_id'])+1

		##
		self.m_dbm = dbmanage()
		self.stop_flag = False
		print('Start Telebot.')
		self.start()

	def __del__(self):
		print('End Telebot')
		self.stop_flag = True
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			try: del self.users_data[id_str]['inst']
			except: pass

	### msg_loop
	def run(self):
		while self.stop_flag is not True:
			### 메시지 받는부분
			for idx,md in enumerate(range(len(self.users_data))):
				id_str = 'user'+ str(idx+1)
				aupdate = None
				try: aupdate = self.users_data[id_str]['inst'].getUpdates(self.users_data[id_str]['offset'])
				except: continue
				if len(aupdate) == 0 :
					time.sleep(0.25)
					continue
				else: #새메시지
					for row in aupdate:
						self.users_data[id_str]['offset'] = int(row['update_id'])+1
						aMessage,aid = '',''						
						try: 
							aMessage  = row['message']['text']
							aid = row['message']['from']['id']							
						except: 
							print('메시지와아이디 삽입 실패')
							continue		
						self.got_msg_handle(aid,aMessage)
					time.sleep(0.15)

	#가저온 메시지 처리부분
	def got_msg_handle(self,aid,aMessage):				
		#회사코드로 검색을 요청한 경우
		if sUtil.is_only_digit(str(aMessage)):
			num_list = self.m_dbm.get_corp_codes(str(aMessage))
			mk_words = ''

			if len(num_list) == 0:
				self.sendMessage(aid,'해당 종목을 찾지 못하였습니다.')				
			else:
				for nrow in num_list: #한개만 왔을꺼임					
					mk_words += self.get_corp_info(str(nrow[0]),str(nrow[1]))
				photo_url = 'http://cichart.moneta.kr/pax/chart/candleChart/V201200/paxCandleChartV201200Daily.jsp?abbrSymbol=' + str(aMessage)
				self.sendPhoto(aid,photo_url,'')
				self.sendMessage(aid,mk_words)
		else: #회사명으로 검색을 요청한 경우
			ret_list = self.m_dbm.get_corp_names(str(aMessage))
			if len(ret_list) == 0:
				self.sendMessage(aid,"'"+ str(aMessage) +"' 의 검색결과 해당 회사명이 존재하지 않습니다.")				
			else:
				ret_word = ""
				for row in ret_list:
					tw = str(row[0])+ '('+ str(row[1]) +')'+ ' ' + str(row[2]) + '\r\n\r\n'
					ret_word += tw
				self.sendMessage(aid,ret_word)

	#
	def sendMessage(self,aid,aMessage):
		preString = "https://api.telegram.org/bot"
		beforeidString = "/sendmessage?chat_id="
		beforeText = "&text="
		sid = str(aid)
		stoken = ""
		sMessage = aMessage
		#아이디로 토큰찾기
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			if str(self.users_data[id_str]['id']) == str(sid):
				stoken = self.users_data[id_str]['token']
				break		
		ret_word = preString + stoken + beforeidString + sid + beforeText + sMessage		
		try:
			arv = requests.get(ret_word)
			if '200' not in str(arv):
				print(arv)
				print(str(sid) + '님께 : '+ sMessage + ' 전송 실패 재전송합니다.')
				arv = requests.get(ret_word)
		except:
			try:
				print(str(sid) + '님께 : '+ sMessage + ' 전송 실패 재전송합니다.')
				arv = requests.get(ret_word)
			except:	print('메시지 전송에 실패하였습니다.')
	
	#	
	def sendPhoto(self,aid,apurl,acaption):
		preString = "https://api.telegram.org/bot"
		beforeidString = "/sendphoto?chat_id="
		beforeText = "&caption="
		beforeImg = "&photo="
		simg = apurl
		sid = str(aid)
		stoken = ""
		sMessage = acaption
		#아이디로 토큰찾기
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			if str(self.users_data[id_str]['id']) == str(sid):
				stoken = self.users_data[id_str]['token']
				break		
		ret_word = preString + stoken + beforeidString + sid + beforeImg + simg + beforeText + sMessage
		try:
			arv = requests.get(ret_word)
			if '200' not in str(arv):
				print(str(sid) + '님께 이미지전송 실패 재전송합니다.')
				arv = requests.get(ret_word)
		except:
			try:
				print(str(sid) + '님께 이미지전송 실패 재전송합니다.')
				arv = requests.get(ret_word)
			except:	print('이미지 전송에 실패하였습니다.')

	def sendMessage_all(self,astr):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)			
			self.sendMessage(self.users_data[id_str]['id'],astr)			

	def sendPhoto_all(self,aurl,amessage):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			self.sendPhoto(self.users_data[id_str]['id'],aurl,amessage)	

	#회사정보 추출
	def get_corp_info(self,aname,acode):
		request_word = 'http://finance.naver.com/item/coinfo.nhn?code='
		request_word += acode
		ret_list = []
		ret_list.append(acode)
		req = requests.get(request_word)
		html = req.text
		asoup = soup(html, 'html.parser')

		#[현재가,거래량,시가총액,총주식수,52주최고,52주최저,PER,EPS]
		#현재가,거래량
		pdiv = asoup.find_all("div",{"class","new_totalinfo"})
		for ppdiv in pdiv:
			 for adl in ppdiv.find_all("dl"):
			 	for add in adl.find_all("dd"):
			 		try:
			 			if "현재가" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 			if "거래량" in add.text:
			 				ret_list.append(sUtil.make_digit(add.text.split()[1]))
			 		except ValueError: pass

		#시가총액,총주식수,52주최고,52주최저,PER,EPS
		ncounter = 0
		pdiv = asoup.find_all("div",{"id","aside_invest_info"})
		for ppdiv in pdiv:
			for ptable in ppdiv.find_all("table"):
				for ptd in ptable.find_all("td"):
					for pem in ptd.find_all("em"):
						try:
							if [0,2,12,13,14,15].index(ncounter) >= 0 :
								ret_list.append(sUtil.make_digit(pem.text))
						except ValueError: pass
						ncounter+=1

		add_coinfo = ''
		pdiv = asoup.find_all("div",{"id","summary_info"})
		for ppdiv in pdiv:
			for ptag in ppdiv.find_all("p"):
				add_coinfo += ptag.text
				
		#사번 현재가 거래량 시가총액(억) 상장주식수 52주최고 52주최저 PER EPS
		ret_info_words = ''
		ret_info_words += aname + '('+ acode + ')' + '\r\n'
		ret_info_words += '현재가 = ' + ret_list[1] + '\r\n'
		ret_info_words += '현재거래량 = ' + ret_list[2] + '\r\n'
		ret_info_words += '시가총액(억원) = '+ ret_list[3] + '\r\n'
		ret_info_words += '상장주식수 = ' + ret_list[4] + '\r\n'
		ret_info_words += '52주최고가 = ' + ret_list[5] + '\r\n'
		ret_info_words += '52주최저가 = ' + ret_list[6] + '\r\n'
		ret_info_words += 'PER = ' + ret_list[7] + '\r\n'
		ret_info_words += 'EPS = ' + ret_list[8] + '\r\n'
		ret_info_words += '회사개요 = '+'\r\n'+add_coinfo + '\r\n'
		
		return ret_info_words

if __name__ == '__main__':
	#json 파일에는 토큰과 아이디를 넣으셈 여러명 가능
	test = teleBot()
	test.sendPhoto_all('https://khashtamov.com/wp-content/uploads/2016/04/Telegram_logo-300x300.png','Telegram..')
