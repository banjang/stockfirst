from bs4 import BeautifulSoup as soup
import requests,datetime,csv,time
import dbmanager as dbm
from stringutil import sUtil as su
from Schedule import timeUtil as tu
from Schedule import timeschedule as ts

#http://finance.naver.com/item/frgn.nhn?code=053290&page=1
class verifyfilt:
	def __init__(self):
		self.m_dbm = dbm.dbmanage()
		self.m_ts = ts().set_holidays()

	#최근 3개월동안 기관 또는 외국인이 매수를 하고 있어야한다.
	#둘다 팔고 있으면 안된다. 그래서 둘다 내려가는 그래프라면 step4를 step3로 변경해야한다.
	def chk_foreign_income(self,step4_lists):
		#2번째 갭상양봉의 경우 이걸 건드리지 말고 일단 추천주로 이동을 해야할것 같다.
		#여기서 2갭주도 작업(건드리지않는)을 해야한다.
		#그리고 기관의 순매수가 지난 20일을 다 더하고 빼서 순매수가 플러스 1이상이어야한다.
		#그리고 위의것이 아니라면 외국인이라도 매수에 가담을 해야한다. 위의것과 OR조건식이다.
		for row in step4_lists:			
			req_word = 'http://finance.naver.com/item/frgn.nhn?code=' + str(row[1]) + '&page=1'			
			req = requests.get(req_word)
			html = req.content
			asoup = soup(html,'html.parser')
			pdiv = asoup.find_all("table")			
			tot_lay,dtot = [],[]
			for atd in pdiv:
				attr_word = atd.attrs["summary"]
				if ('외국인' in attr_word) and ('기관' in attr_word) and ('순매매' in attr_word):					
					for idx,ar_td in enumerate(atd.find_all('td')):
						irol = ar_td.text.strip()
						if len(irol) < 1: continue
						else: tot_lay.append(irol)
				else: continue

			row_count = 9
			if len(tot_lay) % row_count != 0 :	#한행에 9개씩이다.
				print("20일 기관외국인 도표를 받지 못했습니다.")
				return False
			else:
				dtot = self.divide_list(tot_lay,row_count)				
				ki_sum,fr_sum = 0,0
				for drow in dtot:
					ki_sum += su.make_number(drow[5])
					fr_sum += su.make_number(drow[6])					
				if not (ki_sum > 0 or fr_sum > 0):
					print('기관 외국인 수급없음')
					self.m_dbm.update_step_down_not_imcome(row[1])

	#지난 3년대비 현재가격
	def chk_low_price_3years(self,step4_lists,time_flag):
		for row in step4_lists:			
			print(row[0]+' 3년대비 저가확인중..')			
			get_list = []
			get_list = self.get_3year_data(row)
			#조회 불가능인
			if get_list == [] :
				print(row[0]+'조회 불가능')
				continue

			#get_list가 금일을 포함하지 않은경우에 네이버에서 금일을 추가해서 진행한다. 
			if time_flag == 2: #2차시그널일경우
				if not get_list[-1][0] == tu.get_today_word():
					#조회가능한날이였을때 까지 검사해야함  #오전장시작전이였을때는 붙히지 않는다.
					if self.m_ts.istoday_sday() == False and self.m_ts.istoday_holiday() == False:
						gtoday = self.append_today_ril((row[0],row[1]))
						if not 'None' in gtoday: get_list.append(gtoday)

			#3년을 조회했는데 최소6개월은 되야 뭐가됨..
			if len(get_list) < 479 : print("거래일이 2년 미만임")		
			print(len(get_list))

			#마지막것이 최신이다.
			#만들어진 get_list의 구성 : #리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
			end_prices = []
			for grow in get_list: end_prices.append(float(grow[4]))

			gap4th = min(end_prices) + ((max(end_prices) - min(end_prices)) *0.6)
			#print(end_prices[-1])
			#print(gap4th)
			if end_prices[-1] > gap4th:	#지난3년대비 최저가의 60%보다 높은거임
				print('지난 3년대비 최저가의 60%보다 높음')
				self.m_dbm.update_manage_text_too_high(row[1])

##################
	def get_3year_data(self,acoinfo):
		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
		#날짜 종가 시가 고가 저가 등락가 등락율 거래량  팍스넷은 #약25 페이지정도가 3년이다. 		
		req_words = ['http://paxnet.moneta.co.kr/stock/stockIntro/stockPrice/immedStockList.jsp?code=','&szLowPage=']
		page_index = 1
		max_page = 25
		all_list,div_list,made_list = [],[],[]		
		print('%s 지난 3년간 데이터 조회중 입니다.'%(str(acoinfo[0])))
		while True:
			time.sleep(0.3)
			req_word = req_words[0] + str(acoinfo[1]) + req_words[1] + str(page_index)
			req = requests.get(req_word)
			html = req.content
			asoup = soup(html,'html.parser')
			ptd = asoup.find_all("td",{"class","basicTi_1202"})
			for atd in ptd:
				if '일자별 시세' == atd.text.strip():
					btr = atd.parent.find_next_sibling()
					td_cnt = 0 #해당 td갑 없는경우 중단한다.
					for aintd in btr.find_all("td"):
						try:
							if 'S' in aintd.attrs["class"][0]:
								td_cnt += 1
								all_list.append(aintd.text.strip())
							else: continue						
						except: continue
					if td_cnt == 0 :break
			page_index += 1
			print('.',end='')
			if page_index > max_page : break

		
		if len(all_list) % 8 == 0: div_list = self.divide_list(all_list,8)
		else: 
			print('일자별시세를 받아오지 못함.')
			return []

		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]  로 변경
		for pdiv in div_list: 			
			pdiv[5],pdiv[7] = pdiv[7], pdiv[5]
			pdiv[1],pdiv[2] = pdiv[2], pdiv[1]			
			pdiv[2],pdiv[4] = pdiv[4], pdiv[2]			
			pdiv[2],pdiv[3] = pdiv[3], pdiv[2]
			pdiv.pop()
			pdiv.pop()
			for idx,pprow in enumerate(pdiv):pdiv[idx] = su.make_digit(pprow)			
			made_list.append(pdiv)

		#뒤집어서(최근이 뒤에가도록) 리턴함
		made_list.reverse()
		return made_list

	#오늘날자를 추가한다.
	def append_today_ril(self,co_info):
		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
		ret_list = []
		ret_list.append(tu.get_today_word())
		for rnn in range(5): ret_list.append('None')
		request_word = 'http://finance.naver.com/item/coinfo.nhn?code='
		request_word += co_info[1]
		req = requests.get(request_word)
		html = req.text
		asoup = soup(html, 'html.parser')
		pdl = asoup.find_all("dl",{"class","blind"})
		for pinpdl in pdl:
			for inpdl in pinpdl.find_all("dd"):
				cwd = inpdl.text.strip()
				if '시가' in cwd: ret_list[1] = su.make_digit(cwd)
				if '고가' in cwd: ret_list[2] = su.make_digit(cwd)
				if '저가' in cwd: ret_list[3] = su.make_digit(cwd)
				if '현재가' in cwd and '전일대비' in cwd:
					ret_list[4] = su.make_digit(cwd[cwd.index('현재가')+3:cwd.index('전일대비')])
				if '거래량' in cwd: ret_list[5] = su.make_digit(cwd)

		return ret_list

	#리스트나누기
	def divide_list(self,alist,reg):
		ret_list,append_list,rev_list = [],[],[]
		while alist: rev_list.append(alist.pop())
		idx = 0
		while rev_list:
			append_list.append(rev_list.pop())
			idx += 1
			if idx==reg or len(rev_list) == 0:
				idx = 0
				ret_list.append(append_list)
				append_list = []
		return ret_list
	
if __name__ == '__main__':
	vf = verifyfilt()	
	#vf.chk_foreign_income([['부산가스', '015350', 'KOSPI', 20170717, 48, 4380, 169943, 4850, 2825, 1245, 28426850, '12.17', '360', 3, None]])
	vf.get_3year_data(['현대모비스', '012330', 'KOSPI', 20170717, 48, 4380, 169943, 4850, 2825, 1245, 28426850, '12.17', '360', 3, None])