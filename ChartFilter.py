import datetime,requests,csv,time
#import matplotlib.pyplot as plt
from bs4 import BeautifulSoup as soup
from Schedule import timeUtil as tu
import Schedule as sc
import stringutil as su
import dbmanager as dbm
import NormalChartFilter as ncf

#함수들은 점수 갱신 방식으로 이루어진다.
class chartgrapper:
	def __init__(self):
		self.m_dbm = dbm.dbmanage()
		self.m_ncf = ncf.normalchartfilter()
		self.m_tsc = sc.timeschedule().set_holidays()

	def update_4thFilter(self,pass_finance_list,time_flag):
		for row in pass_finance_list:
			print(row[0]+' 조회중..')
			get_list = []
			if 'KOSPI' in row[2]:				
				get_list = self.get_kospi_list(row)
				time.sleep(1)
			elif 'KOSDAQ' in row[2]:
				#여기서 시간을 잡아먹으니 네이버로 바로점프해서 양봉인지 음봉인지 판단하고 바로 continue 시키자 
				if time_flag == 2:
					if self.is_red_day(row[1]) == False: 
						print('양봉아님')
						continue			
				#아니라면 구글에서 받아온 첫페이지만 검사하자	
				get_list = self.get_kosdaq_list(row,time_flag)

			#조회 불가능인데 이것은 아마도 구글에서 날를 자르는것 같다.. 
			if get_list == [] : #조회불가능은 심각한상황으로 텔레그램으로 알린다.
				print(row[0]+'양봉아님 또는 조회 불가능')
				continue

			#1년을 조회했는데 최소6개월은 되야 뭐가됨..
			if len(get_list) < 119 :
				print("거래일이 6개월 미만임")
				continue

			#(필수)평균거래량(10일)이 1만주 이하인것은 안됨
			if self.get_avr_volume(get_list,10000) == False :
				print("평균거래량 미달")
				continue			

			#get_list가 금일을 포함하지 않은경우에 네이버에서 금일을 추가해서 진행한다. 
			if time_flag == 2: #2차시그널일경우
				if not get_list[-1][0] == sc.timeUtil.get_today_word():
					#조회가능한날이였을때 까지 검사해야함  #오전장시작전이였을때는 붙히지 않는다.
					if self.m_tsc.istoday_sday() == False and self.m_tsc.istoday_holiday() == False:
						gtoday = self.append_today_ril((row[0],row[1]))
						if not 'None' in gtoday: get_list.append(gtoday)
			
			#회사 최고 점수는 51점이다.  재무를 타이트하게 가져가지 않는다.
			#만들어진 get_list의 구성 : #리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]			
			#(필수)그물망 차트  !!그물망 차트해야된다.
			if self.net_web_signal(get_list) == False: 
				#그물망 차트를 통과하지 못하면 다른 일반 기술지표로 검사				
				self.m_ncf.is_other_ok((row[0],row[1]),get_list)
				continue
			else : #급등주 가능성이 있으므로 거래량을 재검사
				#급등주 조건이므로 거래량을 다시본다. 최소 2만주
				if self.get_avr_volume(get_list,20000) == False :
					print("급등주 평균거래량 미달")
					continue

				#(필수)obv70을 체크한다. 분산이 아닌가?
				if self.check_obv_from_csv(get_list) == False : 
					print("obv 분산이 일어남")
					continue

				print("%s 그물망차트 통과"%(row[0],))

			#여기 까지 진행됬으면 20점 추가 획득 나머지는 30점으로 나눠 먹기 한다.
			code_score = 20			

			#(필수&선택)점수 10점
			#macd 와 시그널이 올라가면서 macd_rate영역(-1.5 ~ 1.5)안에 있으면 pass
			macd_result = self.is_macd_signal(get_list)
			if macd_result == 0: #macd상승하지 않음
				print("macd가 상승하지 않음")
				continue
			elif macd_result == 1: #rate안쪽은 아니지만 상승하였 거나 골든크로스임
				code_score += 7
			elif macd_result == 2: #상승도하였고 rate안쪽임
				code_score += 10

			#(선택)점수 10점
			#추가점수로 5프로 이상인가와 거래량 300를 본다.
			if self.is_over5_time(get_list) == True:
				code_score += 10

			#(선택)점수 5점
			#스톡케스틱이(5,3,3) k가d보다 위에있는가?? 리테스팅이 일어나는가..
			if self.is_stockcastic_signal(get_list) == True : 
				code_score += 5

			#(선택)점수 5점
			#RSI 14지표  상승이면서 30~80사이면 pass
			if self.is_rsi_signal(get_list) == True:
				code_score += 5

			if code_score >= 25 : print('%s는 높은점수를 획득함'%(row[0],))
			self.m_dbm.update_corp_score(row[1],code_score)

############################################################################################
	#오늘날짜로 네이버에 가서 양봉이나0%면 True 음봉이면 False를 리턴한다. False면 뒤로 진행하지 않는다.
	def is_red_day(self,acode):
		req_word = 'http://finance.naver.com/item/coinfo.nhn?code=' + str(acode)		
		req = requests.get(req_word)
		html = req.text
		asoup = soup(html, 'html.parser')		
		ret_value = True
		pdiv = asoup.find_all("div",{"class","new_totalinfo"})
		for ppdiv in pdiv:
			 for adl in ppdiv.find_all("dl"):
			 	for add in adl.find_all("dd"):
			 		try:
			 			if "현재가" in add.text:
			 				if "마이너스" in add.text:
			 					ret_value = False
			 		except ValueError: pass		
		time.sleep(0.2)
		return ret_value

	#오늘날자를 추가한다.
	def append_today_ril(self,co_info):
		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
		#예시 20170727 53500.00 53500.00 52200.00 52800.00 942010
		ret_list = []
		ret_list.append(sc.timeUtil.get_today_word())
		for rnn in range(5): ret_list.append('None')
		request_word = 'http://finance.naver.com/item/coinfo.nhn?code='
		request_word += co_info[1]
		req = requests.get(request_word)
		html = req.text
		asoup = soup(html, 'html.parser')
		pdl = asoup.find_all("dl",{"class","blind"})
		for pinpdl in pdl:
			for inpdl in pinpdl.find_all("dd"):
				cwd = inpdl.text.strip()
				if '시가' in cwd: ret_list[1] = su.sUtil.make_digit(cwd)
				if '고가' in cwd: ret_list[2] = su.sUtil.make_digit(cwd)
				if '저가' in cwd: ret_list[3] = su.sUtil.make_digit(cwd)
				if '현재가' in cwd and '전일대비' in cwd:
					ret_list[4] = su.sUtil.make_digit(cwd[cwd.index('현재가')+3:cwd.index('전일대비')])
				if '거래량' in cwd: ret_list[5] = su.sUtil.make_digit(cwd)

		return ret_list

	#5프로이상 상승
	def is_over5_time(self,alist):
		#5프로 이상 상승
		if not (float(alist[-1][4]) / float(alist[-2][4]))  >= 1.05:
			return False

		#거래량 300이상 상승
		vol5_avr = 0
		last_volume = float(alist[-1][5])
		for av in alist[-6:-1]:	vol5_avr += float(av[5])
		if not float(last_volume) >= ((vol5_avr / 5)*3):
			return False

		#지난 20일을 덮는가?
		last_value = alist[-1][4]
		for ai in alist[-21:-1]:
			if not float(last_value) > max(float(ai[1]),float(ai[4])):
				return False
		return True
	
	#이동평균을 구해서 돌려줌
	def make_avr(self,alist,av_number):
		ret_list = []
		for idx,row in enumerate(alist):
			suma = 0.0
			if idx < (av_number-1): 
				for bef in range(idx+1): suma += alist[bef]
				ret_list.append(suma / (idx+1))
				continue
			for af in alist[idx-(av_number-1):idx+1]: suma += af
			ret_list.append(suma / av_number)
		return ret_list

	#그물망 차트 나오는것 잡아내야한다. #마지막날을 검사한다.
	#여기서 검사하는 항목 양봉인가,3~5프로상승인가,지난5~20일보다 높은현재가인가,평균거래량보다 많은가
	#분량이 많으면 cover_days를 내부에서 이용하도록한다.
	def net_web_signal(self,alist):
		end_prices = []
		for idd,dayrow in enumerate(alist): end_prices.append(float(dayrow[4]))
		tot_length = len(end_prices)
		#기간 5 간격 12
		avrlist = []
		perid,term = 5,12
		for tr in range(term): avrlist.append(self.make_avr(end_prices,((tr+1)*perid)))		
	
		#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
		#마지막 날이 장대양봉인가
		if not (float(alist[-1][4]) > float(alist[-1][1])): #양봉
			print("양봉아님")
			return False

		#이조건식 이상해..
		if not (float(alist[-1][4]) / float(alist[-2][4]))  >= 1.03: #3프로 이상 상승
			print("3프로 이상 상승 아님")
			return False

		#그물망에 걸쳐있거나 벗어났는가?
		last_value = float(alist[-1][4])
		last_volume = float(alist[-1][5])
		net_count = 0
		for row in avrlist:
			if  float(last_value) <= float(row[tot_length-1]):
				net_count += 1
		if net_count > 3: 
			print("그물망을 벗어나지 못함")
			return False

		#지난 5일을 덮는가?
		for ai in alist[-6:-1]:
			if not float(last_value) > max(float(ai[1]),float(ai[4])):
				print("지난 5일을 덮지 못함")
				return False

		#지난 5일대비 평균거래량에 200%이상인가?
		vol5_avr = 0
		for av in alist[-6:-1]:	vol5_avr += float(av[5])
		if not float(last_volume) >= ((vol5_avr / 5)*2):
			print("지난 5거래일 대비 거래량 200% 상승아님")
			return False 
			
		#현재가가 높은지는 뒤에서 판단함		
		return True

	#RSI 14일지표  상승하면서 30~80이면 True  RSI틀렸다 2일 연속으로 보는게 아닌것 같다.
	def is_rsi_signal(self,alist):
		#RSI = n일간 상승폭의 합계 / (n일간 상승폭의 합계 + n일간 하락폭의 합계) * 100
		end_prices = []
		for dayrow in alist: end_prices.append(float(dayrow[4]))		
		rs = 14
		rsi = []
		lv80,lv30 = [],[]
		for idx,edp in enumerate(end_prices):
			lv80.append(80.0)
			lv30.append(30.0)
			if idx < rs:	#15개가 있어야 14일이동평균이 나온다.
				rsi.append(0.0)
				continue
			#14부터 시작한다.
			add,sub = 0,0
			parea = end_prices[idx-14:idx+1]
			for ix,val in enumerate(parea):
				if ix == 0: continue
				gap = parea[ix] - parea[ix-1]
				if gap >= 0: add += gap
				else: sub += abs(gap)
			if (add + sub) == 0 : rsi.append(0.0)
			else : rsi.append( add / (add+sub) * 100)

		ret_flag = False
		lr = []		
		lr.append(rsi[len(rsi)-2])
		lr.append(rsi[len(rsi)-1])
		if lr[0] < lr[1] and lr[1] < 81 and lr[1] > 29:
				ret_flag = True
		return ret_flag
	
	#스토캐스틱 (15,5,3)(M,N1,N2) 으로 작업한다.  
	#이거 특이한 스토케스틱이다 5 3 3 은 굉장히 특이하다 미리 라려주는것이다. 따라서 당일 방생한거는 신뢰도가 낮다
	#k상승이나거 골든크로스라면 True
	def is_stockcastic_signal(self,alist):
		#평활계수 EP  = 2 / (n + 1)
		#kp = (금일종가 - 5일중 최저가) / (5일중최고가 - 5일중최저가) * 100
		#슬로우 스토캐스틱 sk = (kp값 * EP) + 직전sk값 * (1 - EP)
		#슬로우 스토캐스틱 sd = (sk값 * EP) + 직전sd값 * (1 - EP)
		end_prices,min_prices,max_prices = [],[],[]		
		for dayrow in alist: 
			max_prices.append(float(dayrow[2]))		
			min_prices.append(float(dayrow[3]))
			end_prices.append(float(dayrow[4]))
		##
		M,N1,N2 = 5,3,3
		fk,fd,sd = [],[],[]
		EP = 2 / (N1 + 1)
		for idx,ep in enumerate(end_prices):
			if idx < (M-1) :
				fk.append(0.0)
				fd.append(0.0)			
				sd.append(0.0)
				continue
			
			fk_top = (end_prices[idx] - min(min_prices[idx-(M-1):idx+1]))
			fk_base = (max(max_prices[idx-(M-1):idx+1]) - min(min_prices[idx-(M-1):idx+1]))				
			if fk_base == 0 : fk.append(100.0)		
			else:
				pfk = fk_top / fk_base * 100 
				fk.append(pfk)

			pfd = (pfk * EP) + fd[idx-1] * (1 - EP)
			fd.append(pfd)
			psd = (pfd * EP) + sd[idx-1] * (1 - EP)
			sd.append(psd)

		#마지막 2일의 지표가 상승이거나 마지막날에 골든 크로스가 나면된다.
		ret_flag = False
		lk,ld = [],[]
		lk.append(fd[len(fd)-3])
		lk.append(fd[len(fd)-2])
		lk.append(fd[len(fd)-1])
		
		ld.append(sd[len(sd)-3])
		ld.append(sd[len(sd)-2])
		ld.append(sd[len(sd)-1])

		#2일연속 지표상승했다면 True
		#if lk[0] < lk[1] and lk[1] < lk[2]: ret_flag = True

		#마지막날에 골든크로스를 한번더 검사
		if ret_flag == False:
			lk.reverse()
			lk.pop()
			lk.reverse()
			if ld[1] < lk[1] and lk[0] <= lk[1] and ld[0] >= lk[0]:
				ret_flag = True

		#90이하 어디갔어?
		if lk[1] >= 90 : ret_flag == False
		return ret_flag

	#최근 2일 이상 상승으로 변경하고 그 올라가야하고 return 1 
	#그리고 2일이 macd_rate 영역 안에 있으면 return 2
	def is_macd_signal(self,alist):
		if len(alist) < 1 : return
		#MACD : 12일 지수이동평균 - 26일 지수이동평균
		#시그널 : MACD의 9일 지수이동평균
		#오실레이터 : MACD값 - 시그널값
		#MACD RATE : MACD값 / 종가 * 100  (MACD RATE +- 1.5사이에서 급등이 발생한다.)
		avr12, avr26, macd, signal, macdrate = [],[],[],[],[]
		EP12 = float(2 / (12+1))
		EP26 = float(2 / (26+1))
		SIGEP9 = float(2 / (9+1))

		#12지수평균 26지수평균 MACD와 SIGNAL 값을 삽입한다.
		for idx,row in enumerate(alist):
			if idx == 0:	#첫날엔 지수이동평균가가 종가이다.
				avr12.append(float(alist[idx][4]))
				avr26.append(float(alist[idx][4]))
				macd.append(0.0)
				signal.append(0.0)
				macdrate.append(0.0)
				continue

			avr12.append(float(alist[idx][4])*EP12 + avr12[idx-1]*(1-EP12))
			avr26.append(float(alist[idx][4])*EP26 + avr26[idx-1]*(1-EP26))
			macd.append(avr12[idx] - avr26[idx])
			signal.append(float(macd[idx])*SIGEP9 + float(signal[idx-1])*(1-SIGEP9))
			macdrate.append(float(macd[idx] / float(alist[idx][4]) * 100))

		af,sg,ar = [],[],[]
		af.append(macd[len(macd)-3])
		af.append(macd[len(macd)-2])
		af.append(macd[len(macd)-1])
		sg.append(signal[len(signal)-3])
		sg.append(signal[len(signal)-2])
		sg.append(signal[len(signal)-1])
		ar.append(macdrate[len(macdrate)-1])

		#불통은 3이고 물통은 -2 이다.		
		#시그널과 MACD가 2일연속상승
		ret_flag = 0
		if (af[0] < af[1] and af[1] < af[2]): # and (sg[0] < sg[1] and sg[1] < sg[2]):
			ret_flag = 1
			#골든크로스 이여도 2점
			if sg[2] < af[2] and af[1] <= af[2] and sg[1] >= af[1]: ret_flag = 2
			elif ar[0] >= -1.5 and ar[0] <= 1.5 :	ret_flag = 2			
			elif ar[0] > 3: ret_flag = 1
			elif ar[0] < -2: ret_flag = 0
		return ret_flag

	#마지막꺼 보합해서 리스트 나누어주기
	def devide_list_padd(self,alist,reg):
		ret_list,append_list,rev_list = [],[],[]
		lastpart = alist[-reg:]
		while alist: rev_list.append(alist.pop())
		idx = 0
		while rev_list:
			append_list.append(rev_list.pop())
			idx += 1
			if idx==reg or len(rev_list) == 0:
				idx = 0
				ret_list.append(append_list)
				append_list = []

		if len(ret_list[len(ret_list)-1]) != reg:
			ret_list.pop()
			ret_list.append(lastpart)
		return ret_list

	#1년전 문자열 1년전꺼부터 구해야한다.
	def oneyear_ago(self):
		tt = datetime.datetime.now()
		oneago = tu.dateStamp_comma(tt.year-1,tt.month,tt.day)
		now = tu.dateStamp_comma(tt.year,tt.month,tt.day)		
		return (oneago,now)

	#평균거래량이 1만주 이하인것들은 제외 (가격에 따른 변동이 있어야하는데 이걸 구해야 안다. 2만주가 맞는것 같기도 하다.)
	#2만주로 바꾸고 10일 이내에 2만주이하의 것들이 있으면 위험한거다.
	#최근 10일간 2만주이하로 거래가 있는것들은 제외시킨다
	def get_avr_volume(self,alist,lvol):
		if len(alist) < 11 : return 0
		avr_volume = []
		for row in alist: avr_volume.append(float(row[5]))
		for avol in avr_volume[-11:-1]: 
			if avol < lvol : return False
		return True

	#리스트 [날짜, 시작가, 고가, 저가, 종가, 거래량]
	#점수로 반환한다.
	def check_obv_from_csv(self,chart_list):
		if len(chart_list) < 1 : return 0
		#obv 삽입작업
		obv_list = []
		for idx,row in enumerate(chart_list):
			if idx == 0 :
				row.append(0.0)
				obv_list.append(row)
				continue
			#현재종가 row[4], 현재거래량 row[5], 전일obv obv_list[idx-1][6]
			#당일종가가 전일종가보다 크면 OBV = 전일OBV + 당일거래량
			#당일종가가 전일종가보다 작으면 OBV = 전일OBV - 당일거래량
			#당일종가가 전일종가와 같으면 OBV = 전일 OBV
			curr_end = float(row[4])
			curr_vol = float(row[5])
			bday_obv = float(obv_list[idx-1][6])
			bday_end = float(obv_list[idx-1][4])
			if curr_end > bday_end : row.append(bday_obv + curr_vol)
			elif curr_end < bday_end : row.append(bday_obv - curr_vol)
			elif curr_end == bday_end : row.append(bday_obv)			
			obv_list.append(row)

		#self.draw_simple_graph(obv_list)
		#obv70 매집인지 아닌지 확인한다.
		return self.isgood_obv(obv_list)

	#obv70 매집이 되어있는지 확인한다.
	def isgood_obv(self,adata):
		#6번째것이 obv이다.
		data,showing = [],[]
		for elm in adata : data.append(elm[6])
		for sh in data : showing.append(sh)
		picklength = 4
		pandom = []
		pandom_size = 0
		pass_days = 70 / picklength

		#팬덤 구간씩 짤라서 보기 시작한다.
		devided = self.devide_list_padd(data,picklength)
		histori_top = []
		for idx,part in enumerate(devided):
			if idx == 0 : #처음엔 추가만함
				for el in part : pandom.append(el)
				pandom_size += 1
				continue
			#2번째 팬덤
			ptop,pbot = max(pandom), min(pandom)
			ctop,cbot = max(part), min(part)
			if ctop >= ptop : #일반적인 합류의 경우
				for el in part : pandom.append(el)
				pandom_size += 1
			elif ctop < ptop:	#최고점이 내려갔지만 60프로 수준에서 머문다면 합류시킨다. 대신 저점은 높아야한다.
				if (((ptop - pbot)*0.6)+pbot <= ctop) and cbot >= pbot :
					for el in part : pandom.append(el)
					pandom_size += 1
				else:  #그것마저 안되면 팬덤 초기화 (초기화 할때 고점들을 기록해놓는다.)
					histori_top.append(max(pandom))
					pandom = []
					for el in part : pandom.append(el)
					pandom_size = 1

		ret_flag = False
		#obv70 매집이라면 통과
		if pandom_size >= pass_days: ret_flag = True
		#실패지만 obv40이상이고 마지막 팬덤이 역대최고의 2배이상 높은 팬덤값을 가지고잇으면 통과
		#수식이 역대 그래프중에 음수에서 양수로 전환되기만 하면 무조건 True인거 같다.
		elif pandom_size >= 10 and max(pandom) >= (max(histori_top)*2): ret_flag = True 
		else : ret_flag = False
		
		#Graph Draw
		#plt.plot(showing,linewidth=0.75)
		#plt.plot(pandom,linewidth=0.75)
		#if ret_flag: plt.title("Good passpers %d : pers %d"%(pass_days,pandom_size))
		#else : plt.title("Bad passpers %d : pers %d"%(pass_days,pandom_size))
		#plt.show()

		#17개가 패스 인데 10는되고  팬덤고점이 지난 팬덤 고점의 2배이상이면 True다
		return ret_flag

	#코스피 크롤링
	def get_kospi_list(self,pass_finance_list):
		rw = ['https://www.google.com/finance/historical?q=','&startdate=','&enddate=','&output=csv']
		request_word = rw[0] + pass_finance_list[2] +':'+ pass_finance_list[1] + rw[1] + self.oneyear_ago()[0] + rw[2] + self.oneyear_ago()[1] + rw[3]		
		chart_list = []
		ret_list = []		
		with requests.Session() as ses:
			dcsv = ses.get(request_word)
			decoded_content = dcsv.content.decode('utf-8')
			cr = csv.reader(decoded_content.splitlines(), delimiter=',')
			chart_list = list(cr)
			#트레픽 문제때문에 막힐수도 있다..

		if len(chart_list) > 0 : del chart_list[0]
		for row in chart_list:
			row[0] = sc.timeUtil.make_ymd(row[0])
			#거래가 없던날은 필터링을 해줘야한다. 여기서 
			#예) ['20160811', '-', '-', '-', '24250.00', '0']
			if row[1] == '-' and row[2] == '-' and row[3] == '-' and row[4] != '-':
				row[1]=row[2]=row[3]=row[4]
			ret_list.append(row)
		ret_list.reverse()
		return ret_list

	#코스닥 크롤링
	def get_kosdaq_list(self,pass_finance_list,time_flag):
		rw = ['https://www.google.com/finance/historical?q=','&startdate=','&enddate=']
		oneage = self.oneyear_ago()
		request_word = rw[0] + pass_finance_list[2] +':'+ pass_finance_list[1] + rw[1] + oneage[0] + rw[2] + oneage[1]
		ret_list,reversed_list = [],[]
		page_level = 0
		added_words = ['&start=','&num=30']
		add_words = ''
		
		#time_flag
		while True:
		#반복적으로 해야한다.  startday에 가까워 진다 는걸 캐치 해내야하고 
			time.sleep(0.9)
			if page_level != 0:
				add_words = added_words[0] + str(page_level * 30) + added_words[1]			
			
			req = requests.get(request_word+add_words)
			html = req.content
			asoup = soup(html,'html.parser')
			pdiv = asoup.find("table",{"class","historical_price"})
			if pdiv == None : break
			atd  = pdiv.find_next('td')
			#date, open, high, low, close, volume			
			content = atd.text.splitlines()
			#6개의 요소씩 넣는다..
			add_element = []
			for idx,cont in enumerate(content):
				if len(cont) < 1: continue
				if idx == 0 :
					add_element.append(cont)
					continue
				if cont[0:3] in sc.timeUtil.month_words:
					ret_list.append(add_element)
					add_element = []
					add_element.append(cont)
					continue
				
				add_element.append(cont)
				if idx == len(content)-1 : ret_list.append(add_element)

			print('.',end='')
			#오전에 구글에서만 받아올때 첫페이지만보고 양봉인지 아닌지 판단하여 리턴한다.
			if time_flag == 1 and page_level == 0:
				if su.sUtil.make_number(ret_list[0][1]) > su.sUtil.make_number(ret_list[0][4]):					
					return []				
			page_level += 1


		#최신부터 과거순으로 들어가짐 바꺼야함
		for row in ret_list:
			row = su.sUtil.removecomma_list(row)			
			row[0] = sc.timeUtil.engtodate(row[0])
			#거래가 없던날은 필터링을 해줘야한다. 여기서 
			#예) ['20160811', '-', '-', '-', '24250.00', '0']
			if row[1] == '-' and row[2] == '-' and row[3] == '-' and row[4] != '-':
				row[1]=row[2]=row[3]=row[4]
		
			reversed_list.append(row)
		
		reversed_list.reverse()
		return reversed_list

if __name__ == '__main__':
    mychart = chartgrapper()    
    #mychart.update_4thFilter([['상신브레이크', '041650', 'KOSPI', 20170629, 69, 8280, 37070, 12000, 7900, 2867, 34621165, '12.25', '676', 3]])
    #mychart.update_4thFilter([['현대백화점', '069960', 'KOSPI', 20170717, 48, 4380, 169943, 4850, 2825, 1245, 28426850, '12.17', '360', 3, None]],2)
    mychart.update_4thFilter([['대교', '019680', 'KOSPI', 20170629, 70, 13900, 148720, 15000, 8390, 1952, 14046023, '17.03', '816', 3]],2)
    #http://www.google.com/finance/historical?q={"KOSPI:095720"}&startdate={"2017,01,01"}&enddate={"2017,06,29"}&output=csv
	