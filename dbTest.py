#db에서 사용방법에 대해서 서술한다.
import sqlite3
import time,datetime,random
 
conn = sqlite3.connect('tutorial.db')
curr = conn.cursor()

def end_work():
    curr.close()
    conn.close()

#REAL, TEXT, TEXT, REAL
def create_table():
	curr.execute('CREATE TABLE IF NOT EXISTS stuffToPlot(unix REAL, datestamp TEXT, keyword TEXT,value REAL)')

def drop_table():
    curr.execute('DROP TABLE stuffToPlot')

def data_entry():
	c.execute("INSERT INTO stuffToPlot VALUES(2312312.545,'170607','Python',1)")
	conn.commit()  #when you modify table if not you don't have to commit
	curr.close()
	conn.close()

def dynmic_data_entry():
    unix = time.time()
    date = str(datetime.datetime.fromtimestamp(unix).strftime("%Y-%m-%d %H:%M:%S"))
    keyword = 'python'
    value = random.randrange(0,10)
    curr.execute("INSERT INTO stuffToPlot (unix,datestamp, keyword, value) VALUES(?,?,?,?)",(unix,date,keyword,value))
    conn.commit()

def read_from_db():
    curr.execute("SELECT * FROM stuffToPlot WHERE value > 3")
    for row in curr.fetchall():
        print(row)

def del_and_update():
    curr.execute("SELECT * FROM stuffToPlot")
    [print(row) for row in curr.fetchall()]
    #curr.execute("UPDATE stuffToPlot SET value = (?) WHERE value = (?)",x,y)
    curr.execute("UPDATE stuffToPlot SET value = 99 WHERE value = 8")
    conn.commit()

    curr.execute("SELECT * FROM stuffToPlot")
    [print(row) for row in curr.fetchall()]


    curr.execute("DELETE FROM stuffToPlot WHERE value = 99")
    conn.commit()

    curr.execute("SELECT * FROM stuffToPlot")
    [print(row) for row in curr.fetchall()]

if __name__ == '__main__':
    print("go")
    #read_from_db()
    del_and_update()


    #drop_table()
    #create_table()

    '''
    for i in range(50):
        dynmic_data_entry()
        time.sleep(0.25)
        print(i)
    '''
	#data_entry()
    end_work()
