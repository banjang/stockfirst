import os,sys

#run when import
lib_list = ['import requests','import bs4','import telepot']
for row in lib_list:
    try: exec(row)
    except ImportError:        
        print (str(row.split()[1]) + ' module is now Installing..')
        #sys.executable is --> python interpreter
        pre_inst = str(sys.executable) +' -m pip install ' + row.split()[1]
        os.system(pre_inst)
        exec(row)

if __name__ == '__main__':
    print('Just import')
